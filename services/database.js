const mysql = require('mysql2/promise');
const dbConfig = require('../config/database');
const ferrores = require('../db_apis/errores');

async function initialize() {
    const pool = await mysql.createPool(dbConfig.pool);
}

module.exports.initialize = initialize;

async function simpleExecute(statement, binds) {
    let conn;
    
    try {
        conn = await mysql.createConnection(dbConfig.pool);
        
        const result = await conn.query(statement, binds);
        
        return result;
    } catch(err) {
        let errorNuevo = {
            fecha: new Date().toISOString().slice(0, 19).replace('T', ' '),
            id_producto: -1,
            nivel_error: 'Sistema',
            descripcion_error: 'Error al ejecutar la sentencia SQL',
            stacktrace: 'Error dice: ' + err,
            tipo_accion: 'query',
            origen: 'Microservicio de Lectura Producto'
        }
        
        errorNuevo = await ferrores.crear(errorNuevo);
        console.error(err);
    } finally {
        if(conn) {
            try {
                await conn.end();
            } catch(err) {
                console.log(err);
            }
        }
    }
}

module.exports.simpleExecute = simpleExecute;