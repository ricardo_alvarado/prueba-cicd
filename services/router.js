const express = require('express');
const router = new express.Router();
const productos = require('../controllers/productos');

router.route('/productos/:id?')
    .get(productos.get)
    .post(productos.post)
    .put(productos.put)
    .delete(productos.destroy);

module.exports = router;