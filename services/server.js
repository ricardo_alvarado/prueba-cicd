const express = require('express');
const http = require('http');
const webServerConfig = require('../config/server');
const router = require('./router');
const morgan = require('morgan');
const cors = require('cors');
const bodyParser = require('body-parser');
const multer = require('multer');

let httpServer;

function initialize() {
    return new Promise((resolve, reject) => {
        const app = express();
        httpServer = http.createServer(app);
        const storage = multer.diskStorage({
                    destination: (req, res, callback) => {
                        callback(null, './uploads')
                    },
                    filename: (req, file, callback) => {
                        callback(null, file.originalname)
                    }
                });
        const upload = multer({ storage: storage });
        app.use('/uploads', express.static('uploads'));
        app.use(cors());
        app.use(morgan('combined'));
        app.use(express.json({
            reviver: reviveJson,
        }));
        app.use('/api', router);
        app.use(bodyParser.urlencoded({ extended: false }));
        app.use(bodyParser.json());
        
        app.get('/', (req, res) => {
            res.json("hello world");
        });
        
        httpServer.listen(webServerConfig.port)
            .on('listening', () => {
                console.log(`Servidor escuchando en localhost:${webServerConfig.port}`)

                resolve()
            })
            .on('err', () => {
                reject(err);
            });
    });
}

module.exports.initialize = initialize;

const iso8601RegExp = /^\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}(\.\d{3})?Z$/

function reviveJson(key, value) {
    if (typeof value === 'string' && iso8601RegExp.test(value)) {
      return new Date(value)
    } else {
      return value
    }
}