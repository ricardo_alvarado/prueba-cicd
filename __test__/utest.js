let chai = require('chai');
let chaiHttp = require('chai-http');
const app = require('../app');

chai.use(chaiHttp);

const url = app;

describe('obtener un producto', async () => {
    it('retorna estado 404', (done) => {
        chai.request(url)
            .get("/api/productos/999")
            .end((err, res) => {
                console.log(res.body);
                chai.expect(res).to.have.status(404);
                done();
            });
    });
});