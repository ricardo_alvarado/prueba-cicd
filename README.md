
# Servicio de productos

## **Convención de nombres:**

Para el servicio de productos tanto en json como en rutas se utilizó la convención de nombre snake_case.

**_Snake case_** _es la convención que compone las palabras separadas por barra baja (underscore) en vez de espacios y con la primera letra de cada palabra en minúscula. Por ejemplo mi_blog_de_desarrollo._
## Servicio GET
**Nombre del servicio:** Productos

**Nombre del método:** productos

**URL:** /api/productos/:id?

**Tipo de HTTP Method:** GET

**Definición de atributos JSON de respuesta:**

<table class=MsoTable15Grid4Accent5 border=1 cellspacing=0 cellpadding=0
 style='border-collapse:collapse;border:none;mso-border-alt:solid #8EAADB .5pt;
 mso-border-themecolor:accent5;mso-border-themetint:153;mso-yfti-tbllook:1184;
 mso-padding-alt:0cm 5.4pt 0cm 5.4pt'>
 <tr style='mso-yfti-irow:-1;mso-yfti-firstrow:yes;mso-yfti-lastfirstrow:yes'>
  <td width=124 valign=top style='width:88.25pt;border:solid #4472C4 1.0pt;
  mso-border-themecolor:accent5;border-right:none;mso-border-top-alt:solid #4472C4 .5pt;
  mso-border-top-themecolor:accent5;mso-border-left-alt:solid #4472C4 .5pt;
  mso-border-left-themecolor:accent5;mso-border-bottom-alt:solid #4472C4 .5pt;
  mso-border-bottom-themecolor:accent5;background:#4472C4;mso-background-themecolor:
  accent5;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal;mso-yfti-cnfc:5'><b><span
  style='color:white;mso-themecolor:background1'>ATRIBUTO<o:p></o:p></span></b></p>
  </td>
  <td width=105 valign=top style='width:88.25pt;border-top:solid #4472C4 1.0pt;
  mso-border-top-themecolor:accent5;border-left:none;border-bottom:solid #4472C4 1.0pt;
  mso-border-bottom-themecolor:accent5;border-right:none;mso-border-top-alt:
  solid #4472C4 .5pt;mso-border-top-themecolor:accent5;mso-border-bottom-alt:
  solid #4472C4 .5pt;mso-border-bottom-themecolor:accent5;background:#4472C4;
  mso-background-themecolor:accent5;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal;mso-yfti-cnfc:1'><span style='color:
  white;mso-themecolor:background1;mso-bidi-font-weight:bold'>TIPO DE DATO<o:p></o:p></span></p>
  </td>
  <td width=110 valign=top style='width:88.3pt;border-top:solid #4472C4 1.0pt;
  mso-border-top-themecolor:accent5;border-left:none;border-bottom:solid #4472C4 1.0pt;
  mso-border-bottom-themecolor:accent5;border-right:none;mso-border-top-alt:
  solid #4472C4 .5pt;mso-border-top-themecolor:accent5;mso-border-bottom-alt:
  solid #4472C4 .5pt;mso-border-bottom-themecolor:accent5;background:#4472C4;
  mso-background-themecolor:accent5;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal;mso-yfti-cnfc:1'><span style='color:
  white;mso-themecolor:background1;mso-bidi-font-weight:bold'>LONGITUD MAX<o:p></o:p></span></p>
  </td>
  <td width=114 valign=top style='width:88.3pt;border-top:solid #4472C4 1.0pt;
  mso-border-top-themecolor:accent5;border-left:none;border-bottom:solid #4472C4 1.0pt;
  mso-border-bottom-themecolor:accent5;border-right:none;mso-border-top-alt:
  solid #4472C4 .5pt;mso-border-top-themecolor:accent5;mso-border-bottom-alt:
  solid #4472C4 .5pt;mso-border-bottom-themecolor:accent5;background:#4472C4;
  mso-background-themecolor:accent5;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal;mso-yfti-cnfc:1'><span style='color:
  white;mso-themecolor:background1;mso-bidi-font-weight:bold'>OBLIGATORIO<o:p></o:p></span></p>
  </td>
  <td width=113 valign=top style='width:88.3pt;border:solid #4472C4 1.0pt;
  mso-border-themecolor:accent5;border-left:none;mso-border-top-alt:solid #4472C4 .5pt;
  mso-border-top-themecolor:accent5;mso-border-bottom-alt:solid #4472C4 .5pt;
  mso-border-bottom-themecolor:accent5;mso-border-right-alt:solid #4472C4 .5pt;
  mso-border-right-themecolor:accent5;background:#4472C4;mso-background-themecolor:
  accent5;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal;mso-yfti-cnfc:1'><span style='color:
  white;mso-themecolor:background1;mso-bidi-font-weight:bold'>DESCRIPCIÓN<o:p></o:p></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:0'>
  <td width=124 valign=top style='width:88.25pt;border:solid #8EAADB 1.0pt;
  mso-border-themecolor:accent5;mso-border-themetint:153;border-top:none;
  mso-border-top-alt:solid #8EAADB .5pt;mso-border-top-themecolor:accent5;
  mso-border-top-themetint:153;mso-border-alt:solid #8EAADB .5pt;mso-border-themecolor:
  accent5;mso-border-themetint:153;background:#D9E2F3;mso-background-themecolor:
  accent5;mso-background-themetint:51;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;mso-yfti-cnfc:68'><span style='mso-bidi-font-weight:bold'>id<o:p></o:p></span></p>
  </td>
  <td width=105 valign=top style='width:88.25pt;border-top:none;border-left:
  none;border-bottom:solid #8EAADB 1.0pt;mso-border-bottom-themecolor:accent5;
  mso-border-bottom-themetint:153;border-right:solid #8EAADB 1.0pt;mso-border-right-themecolor:
  accent5;mso-border-right-themetint:153;mso-border-top-alt:solid #8EAADB .5pt;
  mso-border-top-themecolor:accent5;mso-border-top-themetint:153;mso-border-left-alt:
  solid #8EAADB .5pt;mso-border-left-themecolor:accent5;mso-border-left-themetint:
  153;mso-border-alt:solid #8EAADB .5pt;mso-border-themecolor:accent5;
  mso-border-themetint:153;background:#D9E2F3;mso-background-themecolor:accent5;
  mso-background-themetint:51;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;mso-yfti-cnfc:64'><span class=SpellE>Int</span></p>
  </td>
  <td width=110 valign=top style='width:88.3pt;border-top:none;border-left:
  none;border-bottom:solid #8EAADB 1.0pt;mso-border-bottom-themecolor:accent5;
  mso-border-bottom-themetint:153;border-right:solid #8EAADB 1.0pt;mso-border-right-themecolor:
  accent5;mso-border-right-themetint:153;mso-border-top-alt:solid #8EAADB .5pt;
  mso-border-top-themecolor:accent5;mso-border-top-themetint:153;mso-border-left-alt:
  solid #8EAADB .5pt;mso-border-left-themecolor:accent5;mso-border-left-themetint:
  153;mso-border-alt:solid #8EAADB .5pt;mso-border-themecolor:accent5;
  mso-border-themetint:153;background:#D9E2F3;mso-background-themecolor:accent5;
  mso-background-themetint:51;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;mso-yfti-cnfc:64'><b style='mso-bidi-font-weight:normal'>-<o:p></o:p></b></p>
  </td>
  <td width=114 valign=top style='width:88.3pt;border-top:none;border-left:
  none;border-bottom:solid #8EAADB 1.0pt;mso-border-bottom-themecolor:accent5;
  mso-border-bottom-themetint:153;border-right:solid #8EAADB 1.0pt;mso-border-right-themecolor:
  accent5;mso-border-right-themetint:153;mso-border-top-alt:solid #8EAADB .5pt;
  mso-border-top-themecolor:accent5;mso-border-top-themetint:153;mso-border-left-alt:
  solid #8EAADB .5pt;mso-border-left-themecolor:accent5;mso-border-left-themetint:
  153;mso-border-alt:solid #8EAADB .5pt;mso-border-themecolor:accent5;
  mso-border-themetint:153;background:#D9E2F3;mso-background-themecolor:accent5;
  mso-background-themetint:51;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;mso-yfti-cnfc:64'><b style='mso-bidi-font-weight:normal'>Si<o:p></o:p></b></p>
  </td>
  <td width=113 valign=top style='width:88.3pt;border-top:none;border-left:
  none;border-bottom:solid #8EAADB 1.0pt;mso-border-bottom-themecolor:accent5;
  mso-border-bottom-themetint:153;border-right:solid #8EAADB 1.0pt;mso-border-right-themecolor:
  accent5;mso-border-right-themetint:153;mso-border-top-alt:solid #8EAADB .5pt;
  mso-border-top-themecolor:accent5;mso-border-top-themetint:153;mso-border-left-alt:
  solid #8EAADB .5pt;mso-border-left-themecolor:accent5;mso-border-left-themetint:
  153;mso-border-alt:solid #8EAADB .5pt;mso-border-themecolor:accent5;
  mso-border-themetint:153;background:#D9E2F3;mso-background-themecolor:accent5;
  mso-background-themetint:51;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;mso-yfti-cnfc:64'>Identificador del producto en la tabla producto</p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:1'>
  <td width=124 valign=top style='width:88.25pt;border:solid #8EAADB 1.0pt;
  mso-border-themecolor:accent5;mso-border-themetint:153;border-top:none;
  mso-border-top-alt:solid #8EAADB .5pt;mso-border-top-themecolor:accent5;
  mso-border-top-themetint:153;mso-border-alt:solid #8EAADB .5pt;mso-border-themecolor:
  accent5;mso-border-themetint:153;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;mso-yfti-cnfc:4'><span class=SpellE><span style='mso-bidi-font-weight:
  bold'>nombre_producto</span></span><span style='mso-bidi-font-weight:bold'><o:p></o:p></span></p>
  </td>
  <td width=105 valign=top style='width:88.25pt;border-top:none;border-left:
  none;border-bottom:solid #8EAADB 1.0pt;mso-border-bottom-themecolor:accent5;
  mso-border-bottom-themetint:153;border-right:solid #8EAADB 1.0pt;mso-border-right-themecolor:
  accent5;mso-border-right-themetint:153;mso-border-top-alt:solid #8EAADB .5pt;
  mso-border-top-themecolor:accent5;mso-border-top-themetint:153;mso-border-left-alt:
  solid #8EAADB .5pt;mso-border-left-themecolor:accent5;mso-border-left-themetint:
  153;mso-border-alt:solid #8EAADB .5pt;mso-border-themecolor:accent5;
  mso-border-themetint:153;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'><span class=SpellE>String</span></p>
  </td>
  <td width=110 valign=top style='width:88.3pt;border-top:none;border-left:
  none;border-bottom:solid #8EAADB 1.0pt;mso-border-bottom-themecolor:accent5;
  mso-border-bottom-themetint:153;border-right:solid #8EAADB 1.0pt;mso-border-right-themecolor:
  accent5;mso-border-right-themetint:153;mso-border-top-alt:solid #8EAADB .5pt;
  mso-border-top-themecolor:accent5;mso-border-top-themetint:153;mso-border-left-alt:
  solid #8EAADB .5pt;mso-border-left-themecolor:accent5;mso-border-left-themetint:
  153;mso-border-alt:solid #8EAADB .5pt;mso-border-themecolor:accent5;
  mso-border-themetint:153;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'>100</p>
  </td>
  <td width=114 valign=top style='width:88.3pt;border-top:none;border-left:
  none;border-bottom:solid #8EAADB 1.0pt;mso-border-bottom-themecolor:accent5;
  mso-border-bottom-themetint:153;border-right:solid #8EAADB 1.0pt;mso-border-right-themecolor:
  accent5;mso-border-right-themetint:153;mso-border-top-alt:solid #8EAADB .5pt;
  mso-border-top-themecolor:accent5;mso-border-top-themetint:153;mso-border-left-alt:
  solid #8EAADB .5pt;mso-border-left-themecolor:accent5;mso-border-left-themetint:
  153;mso-border-alt:solid #8EAADB .5pt;mso-border-themecolor:accent5;
  mso-border-themetint:153;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'>Si</p>
  </td>
  <td width=113 valign=top style='width:88.3pt;border-top:none;border-left:
  none;border-bottom:solid #8EAADB 1.0pt;mso-border-bottom-themecolor:accent5;
  mso-border-bottom-themetint:153;border-right:solid #8EAADB 1.0pt;mso-border-right-themecolor:
  accent5;mso-border-right-themetint:153;mso-border-top-alt:solid #8EAADB .5pt;
  mso-border-top-themecolor:accent5;mso-border-top-themetint:153;mso-border-left-alt:
  solid #8EAADB .5pt;mso-border-left-themecolor:accent5;mso-border-left-themetint:
  153;mso-border-alt:solid #8EAADB .5pt;mso-border-themecolor:accent5;
  mso-border-themetint:153;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'>Nombre del producto</p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:2'>
  <td width=124 valign=top style='width:88.25pt;border:solid #8EAADB 1.0pt;
  mso-border-themecolor:accent5;mso-border-themetint:153;border-top:none;
  mso-border-top-alt:solid #8EAADB .5pt;mso-border-top-themecolor:accent5;
  mso-border-top-themetint:153;mso-border-alt:solid #8EAADB .5pt;mso-border-themecolor:
  accent5;mso-border-themetint:153;background:#D9E2F3;mso-background-themecolor:
  accent5;mso-background-themetint:51;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;mso-yfti-cnfc:68'><span style='mso-bidi-font-weight:bold'>precio<o:p></o:p></span></p>
  </td>
  <td width=105 valign=top style='width:88.25pt;border-top:none;border-left:
  none;border-bottom:solid #8EAADB 1.0pt;mso-border-bottom-themecolor:accent5;
  mso-border-bottom-themetint:153;border-right:solid #8EAADB 1.0pt;mso-border-right-themecolor:
  accent5;mso-border-right-themetint:153;mso-border-top-alt:solid #8EAADB .5pt;
  mso-border-top-themecolor:accent5;mso-border-top-themetint:153;mso-border-left-alt:
  solid #8EAADB .5pt;mso-border-left-themecolor:accent5;mso-border-left-themetint:
  153;mso-border-alt:solid #8EAADB .5pt;mso-border-themecolor:accent5;
  mso-border-themetint:153;background:#D9E2F3;mso-background-themecolor:accent5;
  mso-background-themetint:51;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;mso-yfti-cnfc:64'><span class=SpellE>Double</span></p>
  </td>
  <td width=110 valign=top style='width:88.3pt;border-top:none;border-left:
  none;border-bottom:solid #8EAADB 1.0pt;mso-border-bottom-themecolor:accent5;
  mso-border-bottom-themetint:153;border-right:solid #8EAADB 1.0pt;mso-border-right-themecolor:
  accent5;mso-border-right-themetint:153;mso-border-top-alt:solid #8EAADB .5pt;
  mso-border-top-themecolor:accent5;mso-border-top-themetint:153;mso-border-left-alt:
  solid #8EAADB .5pt;mso-border-left-themecolor:accent5;mso-border-left-themetint:
  153;mso-border-alt:solid #8EAADB .5pt;mso-border-themecolor:accent5;
  mso-border-themetint:153;background:#D9E2F3;mso-background-themecolor:accent5;
  mso-background-themetint:51;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;mso-yfti-cnfc:64'>-</p>
  </td>
  <td width=114 valign=top style='width:88.3pt;border-top:none;border-left:
  none;border-bottom:solid #8EAADB 1.0pt;mso-border-bottom-themecolor:accent5;
  mso-border-bottom-themetint:153;border-right:solid #8EAADB 1.0pt;mso-border-right-themecolor:
  accent5;mso-border-right-themetint:153;mso-border-top-alt:solid #8EAADB .5pt;
  mso-border-top-themecolor:accent5;mso-border-top-themetint:153;mso-border-left-alt:
  solid #8EAADB .5pt;mso-border-left-themecolor:accent5;mso-border-left-themetint:
  153;mso-border-alt:solid #8EAADB .5pt;mso-border-themecolor:accent5;
  mso-border-themetint:153;background:#D9E2F3;mso-background-themecolor:accent5;
  mso-background-themetint:51;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;mso-yfti-cnfc:64'>Si</p>
  </td>
  <td width=113 valign=top style='width:88.3pt;border-top:none;border-left:
  none;border-bottom:solid #8EAADB 1.0pt;mso-border-bottom-themecolor:accent5;
  mso-border-bottom-themetint:153;border-right:solid #8EAADB 1.0pt;mso-border-right-themecolor:
  accent5;mso-border-right-themetint:153;mso-border-top-alt:solid #8EAADB .5pt;
  mso-border-top-themecolor:accent5;mso-border-top-themetint:153;mso-border-left-alt:
  solid #8EAADB .5pt;mso-border-left-themecolor:accent5;mso-border-left-themetint:
  153;mso-border-alt:solid #8EAADB .5pt;mso-border-themecolor:accent5;
  mso-border-themetint:153;background:#D9E2F3;mso-background-themecolor:accent5;
  mso-background-themetint:51;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;mso-yfti-cnfc:64'>Precio del producto</p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:3'>
  <td width=124 valign=top style='width:88.25pt;border:solid #8EAADB 1.0pt;
  mso-border-themecolor:accent5;mso-border-themetint:153;border-top:none;
  mso-border-top-alt:solid #8EAADB .5pt;mso-border-top-themecolor:accent5;
  mso-border-top-themetint:153;mso-border-alt:solid #8EAADB .5pt;mso-border-themecolor:
  accent5;mso-border-themetint:153;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;mso-yfti-cnfc:4'><span class=SpellE><span style='mso-bidi-font-weight:
  bold'>fotografia</span></span><span style='mso-bidi-font-weight:bold'><o:p></o:p></span></p>
  </td>
  <td width=105 valign=top style='width:88.25pt;border-top:none;border-left:
  none;border-bottom:solid #8EAADB 1.0pt;mso-border-bottom-themecolor:accent5;
  mso-border-bottom-themetint:153;border-right:solid #8EAADB 1.0pt;mso-border-right-themecolor:
  accent5;mso-border-right-themetint:153;mso-border-top-alt:solid #8EAADB .5pt;
  mso-border-top-themecolor:accent5;mso-border-top-themetint:153;mso-border-left-alt:
  solid #8EAADB .5pt;mso-border-left-themecolor:accent5;mso-border-left-themetint:
  153;mso-border-alt:solid #8EAADB .5pt;mso-border-themecolor:accent5;
  mso-border-themetint:153;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'><span class=SpellE>String</span></p>
  </td>
  <td width=110 valign=top style='width:88.3pt;border-top:none;border-left:
  none;border-bottom:solid #8EAADB 1.0pt;mso-border-bottom-themecolor:accent5;
  mso-border-bottom-themetint:153;border-right:solid #8EAADB 1.0pt;mso-border-right-themecolor:
  accent5;mso-border-right-themetint:153;mso-border-top-alt:solid #8EAADB .5pt;
  mso-border-top-themecolor:accent5;mso-border-top-themetint:153;mso-border-left-alt:
  solid #8EAADB .5pt;mso-border-left-themecolor:accent5;mso-border-left-themetint:
  153;mso-border-alt:solid #8EAADB .5pt;mso-border-themecolor:accent5;
  mso-border-themetint:153;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'>N</p>
  </td>
  <td width=114 valign=top style='width:88.3pt;border-top:none;border-left:
  none;border-bottom:solid #8EAADB 1.0pt;mso-border-bottom-themecolor:accent5;
  mso-border-bottom-themetint:153;border-right:solid #8EAADB 1.0pt;mso-border-right-themecolor:
  accent5;mso-border-right-themetint:153;mso-border-top-alt:solid #8EAADB .5pt;
  mso-border-top-themecolor:accent5;mso-border-top-themetint:153;mso-border-left-alt:
  solid #8EAADB .5pt;mso-border-left-themecolor:accent5;mso-border-left-themetint:
  153;mso-border-alt:solid #8EAADB .5pt;mso-border-themecolor:accent5;
  mso-border-themetint:153;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'>Si</p>
  </td>
  <td width=113 valign=top style='width:88.3pt;border-top:none;border-left:
  none;border-bottom:solid #8EAADB 1.0pt;mso-border-bottom-themecolor:accent5;
  mso-border-bottom-themetint:153;border-right:solid #8EAADB 1.0pt;mso-border-right-themecolor:
  accent5;mso-border-right-themetint:153;mso-border-top-alt:solid #8EAADB .5pt;
  mso-border-top-themecolor:accent5;mso-border-top-themetint:153;mso-border-left-alt:
  solid #8EAADB .5pt;mso-border-left-themecolor:accent5;mso-border-left-themetint:
  153;mso-border-alt:solid #8EAADB .5pt;mso-border-themecolor:accent5;
  mso-border-themetint:153;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'>Fotografía en base 64 del producto</p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:4'>
  <td width=124 valign=top style='width:88.25pt;border:solid #8EAADB 1.0pt;
  mso-border-themecolor:accent5;mso-border-themetint:153;border-top:none;
  mso-border-top-alt:solid #8EAADB .5pt;mso-border-top-themecolor:accent5;
  mso-border-top-themetint:153;mso-border-alt:solid #8EAADB .5pt;mso-border-themecolor:
  accent5;mso-border-themetint:153;background:#D9E2F3;mso-background-themecolor:
  accent5;mso-background-themetint:51;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;mso-yfti-cnfc:68'><span style='mso-bidi-font-weight:bold'>existencia<o:p></o:p></span></p>
  </td>
  <td width=105 valign=top style='width:88.25pt;border-top:none;border-left:
  none;border-bottom:solid #8EAADB 1.0pt;mso-border-bottom-themecolor:accent5;
  mso-border-bottom-themetint:153;border-right:solid #8EAADB 1.0pt;mso-border-right-themecolor:
  accent5;mso-border-right-themetint:153;mso-border-top-alt:solid #8EAADB .5pt;
  mso-border-top-themecolor:accent5;mso-border-top-themetint:153;mso-border-left-alt:
  solid #8EAADB .5pt;mso-border-left-themecolor:accent5;mso-border-left-themetint:
  153;mso-border-alt:solid #8EAADB .5pt;mso-border-themecolor:accent5;
  mso-border-themetint:153;background:#D9E2F3;mso-background-themecolor:accent5;
  mso-background-themetint:51;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;mso-yfti-cnfc:64'><span class=SpellE>Int</span></p>
  </td>
  <td width=110 valign=top style='width:88.3pt;border-top:none;border-left:
  none;border-bottom:solid #8EAADB 1.0pt;mso-border-bottom-themecolor:accent5;
  mso-border-bottom-themetint:153;border-right:solid #8EAADB 1.0pt;mso-border-right-themecolor:
  accent5;mso-border-right-themetint:153;mso-border-top-alt:solid #8EAADB .5pt;
  mso-border-top-themecolor:accent5;mso-border-top-themetint:153;mso-border-left-alt:
  solid #8EAADB .5pt;mso-border-left-themecolor:accent5;mso-border-left-themetint:
  153;mso-border-alt:solid #8EAADB .5pt;mso-border-themecolor:accent5;
  mso-border-themetint:153;background:#D9E2F3;mso-background-themecolor:accent5;
  mso-background-themetint:51;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;mso-yfti-cnfc:64'>-</p>
  </td>
  <td width=114 valign=top style='width:88.3pt;border-top:none;border-left:
  none;border-bottom:solid #8EAADB 1.0pt;mso-border-bottom-themecolor:accent5;
  mso-border-bottom-themetint:153;border-right:solid #8EAADB 1.0pt;mso-border-right-themecolor:
  accent5;mso-border-right-themetint:153;mso-border-top-alt:solid #8EAADB .5pt;
  mso-border-top-themecolor:accent5;mso-border-top-themetint:153;mso-border-left-alt:
  solid #8EAADB .5pt;mso-border-left-themecolor:accent5;mso-border-left-themetint:
  153;mso-border-alt:solid #8EAADB .5pt;mso-border-themecolor:accent5;
  mso-border-themetint:153;background:#D9E2F3;mso-background-themecolor:accent5;
  mso-background-themetint:51;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;mso-yfti-cnfc:64'>Si</p>
  </td>
  <td width=113 valign=top style='width:88.3pt;border-top:none;border-left:
  none;border-bottom:solid #8EAADB 1.0pt;mso-border-bottom-themecolor:accent5;
  mso-border-bottom-themetint:153;border-right:solid #8EAADB 1.0pt;mso-border-right-themecolor:
  accent5;mso-border-right-themetint:153;mso-border-top-alt:solid #8EAADB .5pt;
  mso-border-top-themecolor:accent5;mso-border-top-themetint:153;mso-border-left-alt:
  solid #8EAADB .5pt;mso-border-left-themecolor:accent5;mso-border-left-themetint:
  153;mso-border-alt:solid #8EAADB .5pt;mso-border-themecolor:accent5;
  mso-border-themetint:153;background:#D9E2F3;mso-background-themecolor:accent5;
  mso-background-themetint:51;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;mso-yfti-cnfc:64'>Si se encuentra disponible el producto</p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:5'>
  <td width=124 valign=top style='width:88.25pt;border:solid #8EAADB 1.0pt;
  mso-border-themecolor:accent5;mso-border-themetint:153;border-top:none;
  mso-border-top-alt:solid #8EAADB .5pt;mso-border-top-themecolor:accent5;
  mso-border-top-themetint:153;mso-border-alt:solid #8EAADB .5pt;mso-border-themecolor:
  accent5;mso-border-themetint:153;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;mso-yfti-cnfc:4'><span class=SpellE><span style='mso-bidi-font-weight:
  bold'>id_tipo_producto</span></span><span style='mso-bidi-font-weight:bold'><o:p></o:p></span></p>
  </td>
  <td width=105 valign=top style='width:88.25pt;border-top:none;border-left:
  none;border-bottom:solid #8EAADB 1.0pt;mso-border-bottom-themecolor:accent5;
  mso-border-bottom-themetint:153;border-right:solid #8EAADB 1.0pt;mso-border-right-themecolor:
  accent5;mso-border-right-themetint:153;mso-border-top-alt:solid #8EAADB .5pt;
  mso-border-top-themecolor:accent5;mso-border-top-themetint:153;mso-border-left-alt:
  solid #8EAADB .5pt;mso-border-left-themecolor:accent5;mso-border-left-themetint:
  153;mso-border-alt:solid #8EAADB .5pt;mso-border-themecolor:accent5;
  mso-border-themetint:153;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'><span class=SpellE>Int</span></p>
  </td>
  <td width=110 valign=top style='width:88.3pt;border-top:none;border-left:
  none;border-bottom:solid #8EAADB 1.0pt;mso-border-bottom-themecolor:accent5;
  mso-border-bottom-themetint:153;border-right:solid #8EAADB 1.0pt;mso-border-right-themecolor:
  accent5;mso-border-right-themetint:153;mso-border-top-alt:solid #8EAADB .5pt;
  mso-border-top-themecolor:accent5;mso-border-top-themetint:153;mso-border-left-alt:
  solid #8EAADB .5pt;mso-border-left-themecolor:accent5;mso-border-left-themetint:
  153;mso-border-alt:solid #8EAADB .5pt;mso-border-themecolor:accent5;
  mso-border-themetint:153;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'>-</p>
  </td>
  <td width=114 valign=top style='width:88.3pt;border-top:none;border-left:
  none;border-bottom:solid #8EAADB 1.0pt;mso-border-bottom-themecolor:accent5;
  mso-border-bottom-themetint:153;border-right:solid #8EAADB 1.0pt;mso-border-right-themecolor:
  accent5;mso-border-right-themetint:153;mso-border-top-alt:solid #8EAADB .5pt;
  mso-border-top-themecolor:accent5;mso-border-top-themetint:153;mso-border-left-alt:
  solid #8EAADB .5pt;mso-border-left-themecolor:accent5;mso-border-left-themetint:
  153;mso-border-alt:solid #8EAADB .5pt;mso-border-themecolor:accent5;
  mso-border-themetint:153;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'>Si</p>
  </td>
  <td width=113 valign=top style='width:88.3pt;border-top:none;border-left:
  none;border-bottom:solid #8EAADB 1.0pt;mso-border-bottom-themecolor:accent5;
  mso-border-bottom-themetint:153;border-right:solid #8EAADB 1.0pt;mso-border-right-themecolor:
  accent5;mso-border-right-themetint:153;mso-border-top-alt:solid #8EAADB .5pt;
  mso-border-top-themecolor:accent5;mso-border-top-themetint:153;mso-border-left-alt:
  solid #8EAADB .5pt;mso-border-left-themecolor:accent5;mso-border-left-themetint:
  153;mso-border-alt:solid #8EAADB .5pt;mso-border-themecolor:accent5;
  mso-border-themetint:153;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'>Identificador del tipo de producto</p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:6;mso-yfti-lastrow:yes'>
  <td width=124 valign=top style='width:88.25pt;border:solid #8EAADB 1.0pt;
  mso-border-themecolor:accent5;mso-border-themetint:153;border-top:none;
  mso-border-top-alt:solid #8EAADB .5pt;mso-border-top-themecolor:accent5;
  mso-border-top-themetint:153;mso-border-alt:solid #8EAADB .5pt;mso-border-themecolor:
  accent5;mso-border-themetint:153;background:#D9E2F3;mso-background-themecolor:
  accent5;mso-background-themetint:51;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;mso-yfti-cnfc:68'><span class=SpellE><span style='mso-bidi-font-weight:
  bold'>tipo_nombre</span></span><span style='mso-bidi-font-weight:bold'><o:p></o:p></span></p>
  </td>
  <td width=105 valign=top style='width:88.25pt;border-top:none;border-left:
  none;border-bottom:solid #8EAADB 1.0pt;mso-border-bottom-themecolor:accent5;
  mso-border-bottom-themetint:153;border-right:solid #8EAADB 1.0pt;mso-border-right-themecolor:
  accent5;mso-border-right-themetint:153;mso-border-top-alt:solid #8EAADB .5pt;
  mso-border-top-themecolor:accent5;mso-border-top-themetint:153;mso-border-left-alt:
  solid #8EAADB .5pt;mso-border-left-themecolor:accent5;mso-border-left-themetint:
  153;mso-border-alt:solid #8EAADB .5pt;mso-border-themecolor:accent5;
  mso-border-themetint:153;background:#D9E2F3;mso-background-themecolor:accent5;
  mso-background-themetint:51;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;mso-yfti-cnfc:64'><span class=SpellE>string</span></p>
  </td>
  <td width=110 valign=top style='width:88.3pt;border-top:none;border-left:
  none;border-bottom:solid #8EAADB 1.0pt;mso-border-bottom-themecolor:accent5;
  mso-border-bottom-themetint:153;border-right:solid #8EAADB 1.0pt;mso-border-right-themecolor:
  accent5;mso-border-right-themetint:153;mso-border-top-alt:solid #8EAADB .5pt;
  mso-border-top-themecolor:accent5;mso-border-top-themetint:153;mso-border-left-alt:
  solid #8EAADB .5pt;mso-border-left-themecolor:accent5;mso-border-left-themetint:
  153;mso-border-alt:solid #8EAADB .5pt;mso-border-themecolor:accent5;
  mso-border-themetint:153;background:#D9E2F3;mso-background-themecolor:accent5;
  mso-background-themetint:51;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;mso-yfti-cnfc:64'>100</p>
  </td>
  <td width=114 valign=top style='width:88.3pt;border-top:none;border-left:
  none;border-bottom:solid #8EAADB 1.0pt;mso-border-bottom-themecolor:accent5;
  mso-border-bottom-themetint:153;border-right:solid #8EAADB 1.0pt;mso-border-right-themecolor:
  accent5;mso-border-right-themetint:153;mso-border-top-alt:solid #8EAADB .5pt;
  mso-border-top-themecolor:accent5;mso-border-top-themetint:153;mso-border-left-alt:
  solid #8EAADB .5pt;mso-border-left-themecolor:accent5;mso-border-left-themetint:
  153;mso-border-alt:solid #8EAADB .5pt;mso-border-themecolor:accent5;
  mso-border-themetint:153;background:#D9E2F3;mso-background-themecolor:accent5;
  mso-background-themetint:51;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;mso-yfti-cnfc:64'>si</p>
  </td>
  <td width=113 valign=top style='width:88.3pt;border-top:none;border-left:
  none;border-bottom:solid #8EAADB 1.0pt;mso-border-bottom-themecolor:accent5;
  mso-border-bottom-themetint:153;border-right:solid #8EAADB 1.0pt;mso-border-right-themecolor:
  accent5;mso-border-right-themetint:153;mso-border-top-alt:solid #8EAADB .5pt;
  mso-border-top-themecolor:accent5;mso-border-top-themetint:153;mso-border-left-alt:
  solid #8EAADB .5pt;mso-border-left-themecolor:accent5;mso-border-left-themetint:
  153;mso-border-alt:solid #8EAADB .5pt;mso-border-themecolor:accent5;
  mso-border-themetint:153;background:#D9E2F3;mso-background-themecolor:accent5;
  mso-background-themetint:51;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;mso-yfti-cnfc:64'>Nombre que recibe el tipo de producto</p>
  </td>
 </tr>
</table>
<![endif]-->

Ejemplo de uso

Entrada:

url: api/productos

JSON: NULL

Salidas:

En caso de éxito:

Http Status: 200

Si no se indica el id:
![enter image description here](https://i.gyazo.com/bf7aba73c2dc8111cb841cb28ccc0e54.png)
Si se indica el id ej: /api/productos/1
Http Status: 200
![enter image description here](https://i.gyazo.com/28ebdaf32464f21ab65545d510cb4aa4.png)

En caso de que el producto no exista:

Http Status: 404

Respuesta:

![enter image description here](https://i.gyazo.com/6b3164d25153b336e67c28e822fa08d7.png)

## Servicio POST

**Nombre del servicio:** Productos

**Nombre del método:** productos

**URL:** /api/productos

**Tipo de HTTP Method:** POST

**Definición de atributos de JSON de entrada:**

<table class=MsoTable15Grid4Accent5 border=1 cellspacing=0 cellpadding=0
 style='border-collapse:collapse;border:none;mso-border-alt:solid #8EAADB .5pt;
 mso-border-themecolor:accent5;mso-border-themetint:153;mso-yfti-tbllook:1184;
 mso-padding-alt:0cm 5.4pt 0cm 5.4pt'>
 <tr style='mso-yfti-irow:-1;mso-yfti-firstrow:yes;mso-yfti-lastfirstrow:yes'>
  <td width=113 valign=top style='width:88.25pt;border:solid #4472C4 1.0pt;
  mso-border-themecolor:accent5;border-right:none;mso-border-top-alt:solid #4472C4 .5pt;
  mso-border-top-themecolor:accent5;mso-border-left-alt:solid #4472C4 .5pt;
  mso-border-left-themecolor:accent5;mso-border-bottom-alt:solid #4472C4 .5pt;
  mso-border-bottom-themecolor:accent5;background:#4472C4;mso-background-themecolor:
  accent5;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal;tab-stops:88.45pt;mso-yfti-cnfc:5'><b><span
  style='color:white;mso-themecolor:background1'>Atributo<o:p></o:p></span></b></p>
  </td>
  <td width=111 valign=top style='width:88.25pt;border-top:solid #4472C4 1.0pt;
  mso-border-top-themecolor:accent5;border-left:none;border-bottom:solid #4472C4 1.0pt;
  mso-border-bottom-themecolor:accent5;border-right:none;mso-border-top-alt:
  solid #4472C4 .5pt;mso-border-top-themecolor:accent5;mso-border-bottom-alt:
  solid #4472C4 .5pt;mso-border-bottom-themecolor:accent5;background:#4472C4;
  mso-background-themecolor:accent5;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal;tab-stops:88.45pt;mso-yfti-cnfc:1'><b><span
  style='color:white;mso-themecolor:background1'>Tipo de dato<o:p></o:p></span></b></p>
  </td>
  <td width=112 valign=top style='width:88.3pt;border-top:solid #4472C4 1.0pt;
  mso-border-top-themecolor:accent5;border-left:none;border-bottom:solid #4472C4 1.0pt;
  mso-border-bottom-themecolor:accent5;border-right:none;mso-border-top-alt:
  solid #4472C4 .5pt;mso-border-top-themecolor:accent5;mso-border-bottom-alt:
  solid #4472C4 .5pt;mso-border-bottom-themecolor:accent5;background:#4472C4;
  mso-background-themecolor:accent5;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal;tab-stops:88.45pt;mso-yfti-cnfc:1'><b><span
  style='color:white;mso-themecolor:background1'>Longitud <span class=SpellE>maxima</span><o:p></o:p></span></b></p>
  </td>
  <td width=114 valign=top style='width:88.3pt;border-top:solid #4472C4 1.0pt;
  mso-border-top-themecolor:accent5;border-left:none;border-bottom:solid #4472C4 1.0pt;
  mso-border-bottom-themecolor:accent5;border-right:none;mso-border-top-alt:
  solid #4472C4 .5pt;mso-border-top-themecolor:accent5;mso-border-bottom-alt:
  solid #4472C4 .5pt;mso-border-bottom-themecolor:accent5;background:#4472C4;
  mso-background-themecolor:accent5;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal;tab-stops:88.45pt;mso-yfti-cnfc:1'><b><span
  style='color:white;mso-themecolor:background1'>obligatorio<o:p></o:p></span></b></p>
  </td>
  <td width=116 valign=top style='width:88.3pt;border:solid #4472C4 1.0pt;
  mso-border-themecolor:accent5;border-left:none;mso-border-top-alt:solid #4472C4 .5pt;
  mso-border-top-themecolor:accent5;mso-border-bottom-alt:solid #4472C4 .5pt;
  mso-border-bottom-themecolor:accent5;mso-border-right-alt:solid #4472C4 .5pt;
  mso-border-right-themecolor:accent5;background:#4472C4;mso-background-themecolor:
  accent5;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal;tab-stops:88.45pt;mso-yfti-cnfc:1'><b><span
  style='color:white;mso-themecolor:background1'>Descripción <o:p></o:p></span></b></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:0'>
  <td width=113 valign=top style='width:88.25pt;border:solid #8EAADB 1.0pt;
  mso-border-themecolor:accent5;mso-border-themetint:153;border-top:none;
  mso-border-top-alt:solid #8EAADB .5pt;mso-border-top-themecolor:accent5;
  mso-border-top-themetint:153;mso-border-alt:solid #8EAADB .5pt;mso-border-themecolor:
  accent5;mso-border-themetint:153;background:#D9E2F3;mso-background-themecolor:
  accent5;mso-background-themetint:51;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;tab-stops:88.45pt;mso-yfti-cnfc:68'><span style='mso-bidi-font-weight:
  bold'>nombre<o:p></o:p></span></p>
  </td>
  <td width=111 valign=top style='width:88.25pt;border-top:none;border-left:
  none;border-bottom:solid #8EAADB 1.0pt;mso-border-bottom-themecolor:accent5;
  mso-border-bottom-themetint:153;border-right:solid #8EAADB 1.0pt;mso-border-right-themecolor:
  accent5;mso-border-right-themetint:153;mso-border-top-alt:solid #8EAADB .5pt;
  mso-border-top-themecolor:accent5;mso-border-top-themetint:153;mso-border-left-alt:
  solid #8EAADB .5pt;mso-border-left-themecolor:accent5;mso-border-left-themetint:
  153;mso-border-alt:solid #8EAADB .5pt;mso-border-themecolor:accent5;
  mso-border-themetint:153;background:#D9E2F3;mso-background-themecolor:accent5;
  mso-background-themetint:51;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;tab-stops:88.45pt;mso-yfti-cnfc:64'><span class=SpellE>string</span></p>
  </td>
  <td width=112 valign=top style='width:88.3pt;border-top:none;border-left:
  none;border-bottom:solid #8EAADB 1.0pt;mso-border-bottom-themecolor:accent5;
  mso-border-bottom-themetint:153;border-right:solid #8EAADB 1.0pt;mso-border-right-themecolor:
  accent5;mso-border-right-themetint:153;mso-border-top-alt:solid #8EAADB .5pt;
  mso-border-top-themecolor:accent5;mso-border-top-themetint:153;mso-border-left-alt:
  solid #8EAADB .5pt;mso-border-left-themecolor:accent5;mso-border-left-themetint:
  153;mso-border-alt:solid #8EAADB .5pt;mso-border-themecolor:accent5;
  mso-border-themetint:153;background:#D9E2F3;mso-background-themecolor:accent5;
  mso-background-themetint:51;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;tab-stops:88.45pt;mso-yfti-cnfc:64'>100</p>
  </td>
  <td width=114 valign=top style='width:88.3pt;border-top:none;border-left:
  none;border-bottom:solid #8EAADB 1.0pt;mso-border-bottom-themecolor:accent5;
  mso-border-bottom-themetint:153;border-right:solid #8EAADB 1.0pt;mso-border-right-themecolor:
  accent5;mso-border-right-themetint:153;mso-border-top-alt:solid #8EAADB .5pt;
  mso-border-top-themecolor:accent5;mso-border-top-themetint:153;mso-border-left-alt:
  solid #8EAADB .5pt;mso-border-left-themecolor:accent5;mso-border-left-themetint:
  153;mso-border-alt:solid #8EAADB .5pt;mso-border-themecolor:accent5;
  mso-border-themetint:153;background:#D9E2F3;mso-background-themecolor:accent5;
  mso-background-themetint:51;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;tab-stops:88.45pt;mso-yfti-cnfc:64'>Si</p>
  </td>
  <td width=116 valign=top style='width:88.3pt;border-top:none;border-left:
  none;border-bottom:solid #8EAADB 1.0pt;mso-border-bottom-themecolor:accent5;
  mso-border-bottom-themetint:153;border-right:solid #8EAADB 1.0pt;mso-border-right-themecolor:
  accent5;mso-border-right-themetint:153;mso-border-top-alt:solid #8EAADB .5pt;
  mso-border-top-themecolor:accent5;mso-border-top-themetint:153;mso-border-left-alt:
  solid #8EAADB .5pt;mso-border-left-themecolor:accent5;mso-border-left-themetint:
  153;mso-border-alt:solid #8EAADB .5pt;mso-border-themecolor:accent5;
  mso-border-themetint:153;background:#D9E2F3;mso-background-themecolor:accent5;
  mso-background-themetint:51;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;tab-stops:88.45pt;mso-yfti-cnfc:64'>Nombre del producto</p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:1'>
  <td width=113 valign=top style='width:88.25pt;border:solid #8EAADB 1.0pt;
  mso-border-themecolor:accent5;mso-border-themetint:153;border-top:none;
  mso-border-top-alt:solid #8EAADB .5pt;mso-border-top-themecolor:accent5;
  mso-border-top-themetint:153;mso-border-alt:solid #8EAADB .5pt;mso-border-themecolor:
  accent5;mso-border-themetint:153;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;tab-stops:88.45pt;mso-yfti-cnfc:4'><span style='mso-bidi-font-weight:
  bold'>precio<o:p></o:p></span></p>
  </td>
  <td width=111 valign=top style='width:88.25pt;border-top:none;border-left:
  none;border-bottom:solid #8EAADB 1.0pt;mso-border-bottom-themecolor:accent5;
  mso-border-bottom-themetint:153;border-right:solid #8EAADB 1.0pt;mso-border-right-themecolor:
  accent5;mso-border-right-themetint:153;mso-border-top-alt:solid #8EAADB .5pt;
  mso-border-top-themecolor:accent5;mso-border-top-themetint:153;mso-border-left-alt:
  solid #8EAADB .5pt;mso-border-left-themecolor:accent5;mso-border-left-themetint:
  153;mso-border-alt:solid #8EAADB .5pt;mso-border-themecolor:accent5;
  mso-border-themetint:153;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;tab-stops:88.45pt'>Doublé</p>
  </td>
  <td width=112 valign=top style='width:88.3pt;border-top:none;border-left:
  none;border-bottom:solid #8EAADB 1.0pt;mso-border-bottom-themecolor:accent5;
  mso-border-bottom-themetint:153;border-right:solid #8EAADB 1.0pt;mso-border-right-themecolor:
  accent5;mso-border-right-themetint:153;mso-border-top-alt:solid #8EAADB .5pt;
  mso-border-top-themecolor:accent5;mso-border-top-themetint:153;mso-border-left-alt:
  solid #8EAADB .5pt;mso-border-left-themecolor:accent5;mso-border-left-themetint:
  153;mso-border-alt:solid #8EAADB .5pt;mso-border-themecolor:accent5;
  mso-border-themetint:153;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;tab-stops:88.45pt'>-</p>
  </td>
  <td width=114 valign=top style='width:88.3pt;border-top:none;border-left:
  none;border-bottom:solid #8EAADB 1.0pt;mso-border-bottom-themecolor:accent5;
  mso-border-bottom-themetint:153;border-right:solid #8EAADB 1.0pt;mso-border-right-themecolor:
  accent5;mso-border-right-themetint:153;mso-border-top-alt:solid #8EAADB .5pt;
  mso-border-top-themecolor:accent5;mso-border-top-themetint:153;mso-border-left-alt:
  solid #8EAADB .5pt;mso-border-left-themecolor:accent5;mso-border-left-themetint:
  153;mso-border-alt:solid #8EAADB .5pt;mso-border-themecolor:accent5;
  mso-border-themetint:153;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;tab-stops:88.45pt'>Si</p>
  </td>
  <td width=116 valign=top style='width:88.3pt;border-top:none;border-left:
  none;border-bottom:solid #8EAADB 1.0pt;mso-border-bottom-themecolor:accent5;
  mso-border-bottom-themetint:153;border-right:solid #8EAADB 1.0pt;mso-border-right-themecolor:
  accent5;mso-border-right-themetint:153;mso-border-top-alt:solid #8EAADB .5pt;
  mso-border-top-themecolor:accent5;mso-border-top-themetint:153;mso-border-left-alt:
  solid #8EAADB .5pt;mso-border-left-themecolor:accent5;mso-border-left-themetint:
  153;mso-border-alt:solid #8EAADB .5pt;mso-border-themecolor:accent5;
  mso-border-themetint:153;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;tab-stops:88.45pt'>Precio del producto</p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:2'>
  <td width=113 valign=top style='width:88.25pt;border:solid #8EAADB 1.0pt;
  mso-border-themecolor:accent5;mso-border-themetint:153;border-top:none;
  mso-border-top-alt:solid #8EAADB .5pt;mso-border-top-themecolor:accent5;
  mso-border-top-themetint:153;mso-border-alt:solid #8EAADB .5pt;mso-border-themecolor:
  accent5;mso-border-themetint:153;background:#D9E2F3;mso-background-themecolor:
  accent5;mso-background-themetint:51;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;tab-stops:88.45pt;mso-yfti-cnfc:68'><span style='mso-bidi-font-weight:
  bold'>foto<o:p></o:p></span></p>
  </td>
  <td width=111 valign=top style='width:88.25pt;border-top:none;border-left:
  none;border-bottom:solid #8EAADB 1.0pt;mso-border-bottom-themecolor:accent5;
  mso-border-bottom-themetint:153;border-right:solid #8EAADB 1.0pt;mso-border-right-themecolor:
  accent5;mso-border-right-themetint:153;mso-border-top-alt:solid #8EAADB .5pt;
  mso-border-top-themecolor:accent5;mso-border-top-themetint:153;mso-border-left-alt:
  solid #8EAADB .5pt;mso-border-left-themecolor:accent5;mso-border-left-themetint:
  153;mso-border-alt:solid #8EAADB .5pt;mso-border-themecolor:accent5;
  mso-border-themetint:153;background:#D9E2F3;mso-background-themecolor:accent5;
  mso-background-themetint:51;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;tab-stops:88.45pt;mso-yfti-cnfc:64'>Blob</p>
  </td>
  <td width=112 valign=top style='width:88.3pt;border-top:none;border-left:
  none;border-bottom:solid #8EAADB 1.0pt;mso-border-bottom-themecolor:accent5;
  mso-border-bottom-themetint:153;border-right:solid #8EAADB 1.0pt;mso-border-right-themecolor:
  accent5;mso-border-right-themetint:153;mso-border-top-alt:solid #8EAADB .5pt;
  mso-border-top-themecolor:accent5;mso-border-top-themetint:153;mso-border-left-alt:
  solid #8EAADB .5pt;mso-border-left-themecolor:accent5;mso-border-left-themetint:
  153;mso-border-alt:solid #8EAADB .5pt;mso-border-themecolor:accent5;
  mso-border-themetint:153;background:#D9E2F3;mso-background-themecolor:accent5;
  mso-background-themetint:51;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;tab-stops:88.45pt;mso-yfti-cnfc:64'>-</p>
  </td>
  <td width=114 valign=top style='width:88.3pt;border-top:none;border-left:
  none;border-bottom:solid #8EAADB 1.0pt;mso-border-bottom-themecolor:accent5;
  mso-border-bottom-themetint:153;border-right:solid #8EAADB 1.0pt;mso-border-right-themecolor:
  accent5;mso-border-right-themetint:153;mso-border-top-alt:solid #8EAADB .5pt;
  mso-border-top-themecolor:accent5;mso-border-top-themetint:153;mso-border-left-alt:
  solid #8EAADB .5pt;mso-border-left-themecolor:accent5;mso-border-left-themetint:
  153;mso-border-alt:solid #8EAADB .5pt;mso-border-themecolor:accent5;
  mso-border-themetint:153;background:#D9E2F3;mso-background-themecolor:accent5;
  mso-background-themetint:51;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;tab-stops:88.45pt;mso-yfti-cnfc:64'>Si</p>
  </td>
  <td width=116 valign=top style='width:88.3pt;border-top:none;border-left:
  none;border-bottom:solid #8EAADB 1.0pt;mso-border-bottom-themecolor:accent5;
  mso-border-bottom-themetint:153;border-right:solid #8EAADB 1.0pt;mso-border-right-themecolor:
  accent5;mso-border-right-themetint:153;mso-border-top-alt:solid #8EAADB .5pt;
  mso-border-top-themecolor:accent5;mso-border-top-themetint:153;mso-border-left-alt:
  solid #8EAADB .5pt;mso-border-left-themecolor:accent5;mso-border-left-themetint:
  153;mso-border-alt:solid #8EAADB .5pt;mso-border-themecolor:accent5;
  mso-border-themetint:153;background:#D9E2F3;mso-background-themecolor:accent5;
  mso-background-themetint:51;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;tab-stops:88.45pt;mso-yfti-cnfc:64'>Fotografía del producto en base 64</p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:3'>
  <td width=113 valign=top style='width:88.25pt;border:solid #8EAADB 1.0pt;
  mso-border-themecolor:accent5;mso-border-themetint:153;border-top:none;
  mso-border-top-alt:solid #8EAADB .5pt;mso-border-top-themecolor:accent5;
  mso-border-top-themetint:153;mso-border-alt:solid #8EAADB .5pt;mso-border-themecolor:
  accent5;mso-border-themetint:153;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;tab-stops:88.45pt;mso-yfti-cnfc:4'><span style='mso-bidi-font-weight:
  bold'>existencia<o:p></o:p></span></p>
  </td>
  <td width=111 valign=top style='width:88.25pt;border-top:none;border-left:
  none;border-bottom:solid #8EAADB 1.0pt;mso-border-bottom-themecolor:accent5;
  mso-border-bottom-themetint:153;border-right:solid #8EAADB 1.0pt;mso-border-right-themecolor:
  accent5;mso-border-right-themetint:153;mso-border-top-alt:solid #8EAADB .5pt;
  mso-border-top-themecolor:accent5;mso-border-top-themetint:153;mso-border-left-alt:
  solid #8EAADB .5pt;mso-border-left-themecolor:accent5;mso-border-left-themetint:
  153;mso-border-alt:solid #8EAADB .5pt;mso-border-themecolor:accent5;
  mso-border-themetint:153;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;tab-stops:88.45pt'><span class=SpellE>Int</span></p>
  </td>
  <td width=112 valign=top style='width:88.3pt;border-top:none;border-left:
  none;border-bottom:solid #8EAADB 1.0pt;mso-border-bottom-themecolor:accent5;
  mso-border-bottom-themetint:153;border-right:solid #8EAADB 1.0pt;mso-border-right-themecolor:
  accent5;mso-border-right-themetint:153;mso-border-top-alt:solid #8EAADB .5pt;
  mso-border-top-themecolor:accent5;mso-border-top-themetint:153;mso-border-left-alt:
  solid #8EAADB .5pt;mso-border-left-themecolor:accent5;mso-border-left-themetint:
  153;mso-border-alt:solid #8EAADB .5pt;mso-border-themecolor:accent5;
  mso-border-themetint:153;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;tab-stops:88.45pt'>-</p>
  </td>
  <td width=114 valign=top style='width:88.3pt;border-top:none;border-left:
  none;border-bottom:solid #8EAADB 1.0pt;mso-border-bottom-themecolor:accent5;
  mso-border-bottom-themetint:153;border-right:solid #8EAADB 1.0pt;mso-border-right-themecolor:
  accent5;mso-border-right-themetint:153;mso-border-top-alt:solid #8EAADB .5pt;
  mso-border-top-themecolor:accent5;mso-border-top-themetint:153;mso-border-left-alt:
  solid #8EAADB .5pt;mso-border-left-themecolor:accent5;mso-border-left-themetint:
  153;mso-border-alt:solid #8EAADB .5pt;mso-border-themecolor:accent5;
  mso-border-themetint:153;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;tab-stops:88.45pt'>Si</p>
  </td>
  <td width=116 valign=top style='width:88.3pt;border-top:none;border-left:
  none;border-bottom:solid #8EAADB 1.0pt;mso-border-bottom-themecolor:accent5;
  mso-border-bottom-themetint:153;border-right:solid #8EAADB 1.0pt;mso-border-right-themecolor:
  accent5;mso-border-right-themetint:153;mso-border-top-alt:solid #8EAADB .5pt;
  mso-border-top-themecolor:accent5;mso-border-top-themetint:153;mso-border-left-alt:
  solid #8EAADB .5pt;mso-border-left-themecolor:accent5;mso-border-left-themetint:
  153;mso-border-alt:solid #8EAADB .5pt;mso-border-themecolor:accent5;
  mso-border-themetint:153;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;tab-stops:88.45pt'>Disponibilidad del producto</p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:4;mso-yfti-lastrow:yes'>
  <td width=113 valign=top style='width:88.25pt;border:solid #8EAADB 1.0pt;
  mso-border-themecolor:accent5;mso-border-themetint:153;border-top:none;
  mso-border-top-alt:solid #8EAADB .5pt;mso-border-top-themecolor:accent5;
  mso-border-top-themetint:153;mso-border-alt:solid #8EAADB .5pt;mso-border-themecolor:
  accent5;mso-border-themetint:153;background:#D9E2F3;mso-background-themecolor:
  accent5;mso-background-themetint:51;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;tab-stops:88.45pt;mso-yfti-cnfc:68'><span style='mso-bidi-font-weight:
  bold'>tipo<o:p></o:p></span></p>
  </td>
  <td width=111 valign=top style='width:88.25pt;border-top:none;border-left:
  none;border-bottom:solid #8EAADB 1.0pt;mso-border-bottom-themecolor:accent5;
  mso-border-bottom-themetint:153;border-right:solid #8EAADB 1.0pt;mso-border-right-themecolor:
  accent5;mso-border-right-themetint:153;mso-border-top-alt:solid #8EAADB .5pt;
  mso-border-top-themecolor:accent5;mso-border-top-themetint:153;mso-border-left-alt:
  solid #8EAADB .5pt;mso-border-left-themecolor:accent5;mso-border-left-themetint:
  153;mso-border-alt:solid #8EAADB .5pt;mso-border-themecolor:accent5;
  mso-border-themetint:153;background:#D9E2F3;mso-background-themecolor:accent5;
  mso-background-themetint:51;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;tab-stops:88.45pt;mso-yfti-cnfc:64'><span class=SpellE>Int</span></p>
  </td>
  <td width=112 valign=top style='width:88.3pt;border-top:none;border-left:
  none;border-bottom:solid #8EAADB 1.0pt;mso-border-bottom-themecolor:accent5;
  mso-border-bottom-themetint:153;border-right:solid #8EAADB 1.0pt;mso-border-right-themecolor:
  accent5;mso-border-right-themetint:153;mso-border-top-alt:solid #8EAADB .5pt;
  mso-border-top-themecolor:accent5;mso-border-top-themetint:153;mso-border-left-alt:
  solid #8EAADB .5pt;mso-border-left-themecolor:accent5;mso-border-left-themetint:
  153;mso-border-alt:solid #8EAADB .5pt;mso-border-themecolor:accent5;
  mso-border-themetint:153;background:#D9E2F3;mso-background-themecolor:accent5;
  mso-background-themetint:51;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;tab-stops:88.45pt;mso-yfti-cnfc:64'>-</p>
  </td>
  <td width=114 valign=top style='width:88.3pt;border-top:none;border-left:
  none;border-bottom:solid #8EAADB 1.0pt;mso-border-bottom-themecolor:accent5;
  mso-border-bottom-themetint:153;border-right:solid #8EAADB 1.0pt;mso-border-right-themecolor:
  accent5;mso-border-right-themetint:153;mso-border-top-alt:solid #8EAADB .5pt;
  mso-border-top-themecolor:accent5;mso-border-top-themetint:153;mso-border-left-alt:
  solid #8EAADB .5pt;mso-border-left-themecolor:accent5;mso-border-left-themetint:
  153;mso-border-alt:solid #8EAADB .5pt;mso-border-themecolor:accent5;
  mso-border-themetint:153;background:#D9E2F3;mso-background-themecolor:accent5;
  mso-background-themetint:51;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;tab-stops:88.45pt;mso-yfti-cnfc:64'>Si</p>
  </td>
  <td width=116 valign=top style='width:88.3pt;border-top:none;border-left:
  none;border-bottom:solid #8EAADB 1.0pt;mso-border-bottom-themecolor:accent5;
  mso-border-bottom-themetint:153;border-right:solid #8EAADB 1.0pt;mso-border-right-themecolor:
  accent5;mso-border-right-themetint:153;mso-border-top-alt:solid #8EAADB .5pt;
  mso-border-top-themecolor:accent5;mso-border-top-themetint:153;mso-border-left-alt:
  solid #8EAADB .5pt;mso-border-left-themecolor:accent5;mso-border-left-themetint:
  153;mso-border-alt:solid #8EAADB .5pt;mso-border-themecolor:accent5;
  mso-border-themetint:153;background:#D9E2F3;mso-background-themecolor:accent5;
  mso-background-themetint:51;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;tab-stops:88.45pt;mso-yfti-cnfc:64'>Id del tipo de producto</p>
  </td>
 </tr>
</table>

**Definición de atributos JSON de respuesta:**

<table class=MsoTable15Grid4Accent5 border=1 cellspacing=0 cellpadding=0
 style='border-collapse:collapse;border:none;mso-border-alt:solid #8EAADB .5pt;
 mso-border-themecolor:accent5;mso-border-themetint:153;mso-yfti-tbllook:1184;
 mso-padding-alt:0cm 5.4pt 0cm 5.4pt'>
 <tr style='mso-yfti-irow:-1;mso-yfti-firstrow:yes;mso-yfti-lastfirstrow:yes'>
  <td width=124 valign=top style='width:88.25pt;border:solid #4472C4 1.0pt;
  mso-border-themecolor:accent5;border-right:none;mso-border-top-alt:solid #4472C4 .5pt;
  mso-border-top-themecolor:accent5;mso-border-left-alt:solid #4472C4 .5pt;
  mso-border-left-themecolor:accent5;mso-border-bottom-alt:solid #4472C4 .5pt;
  mso-border-bottom-themecolor:accent5;background:#4472C4;mso-background-themecolor:
  accent5;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal;tab-stops:88.45pt;mso-yfti-cnfc:5'><span
  style='color:white;mso-themecolor:background1;mso-bidi-font-weight:bold'>Atributo
  <o:p></o:p></span></p>
  </td>
  <td width=107 valign=top style='width:88.25pt;border-top:solid #4472C4 1.0pt;
  mso-border-top-themecolor:accent5;border-left:none;border-bottom:solid #4472C4 1.0pt;
  mso-border-bottom-themecolor:accent5;border-right:none;mso-border-top-alt:
  solid #4472C4 .5pt;mso-border-top-themecolor:accent5;mso-border-bottom-alt:
  solid #4472C4 .5pt;mso-border-bottom-themecolor:accent5;background:#4472C4;
  mso-background-themecolor:accent5;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal;tab-stops:88.45pt;mso-yfti-cnfc:1'><span
  style='color:white;mso-themecolor:background1;mso-bidi-font-weight:bold'>Tipo
  de dato<o:p></o:p></span></p>
  </td>
  <td width=109 valign=top style='width:88.3pt;border-top:solid #4472C4 1.0pt;
  mso-border-top-themecolor:accent5;border-left:none;border-bottom:solid #4472C4 1.0pt;
  mso-border-bottom-themecolor:accent5;border-right:none;mso-border-top-alt:
  solid #4472C4 .5pt;mso-border-top-themecolor:accent5;mso-border-bottom-alt:
  solid #4472C4 .5pt;mso-border-bottom-themecolor:accent5;background:#4472C4;
  mso-background-themecolor:accent5;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal;tab-stops:88.45pt;mso-yfti-cnfc:1'><span
  style='color:white;mso-themecolor:background1;mso-bidi-font-weight:bold'>Longitud
  <span class=SpellE>maxima</span><o:p></o:p></span></p>
  </td>
  <td width=111 valign=top style='width:88.3pt;border-top:solid #4472C4 1.0pt;
  mso-border-top-themecolor:accent5;border-left:none;border-bottom:solid #4472C4 1.0pt;
  mso-border-bottom-themecolor:accent5;border-right:none;mso-border-top-alt:
  solid #4472C4 .5pt;mso-border-top-themecolor:accent5;mso-border-bottom-alt:
  solid #4472C4 .5pt;mso-border-bottom-themecolor:accent5;background:#4472C4;
  mso-background-themecolor:accent5;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal;tab-stops:88.45pt;mso-yfti-cnfc:1'><span
  style='color:white;mso-themecolor:background1;mso-bidi-font-weight:bold'>Obligatorio
  <o:p></o:p></span></p>
  </td>
  <td width=115 valign=top style='width:88.3pt;border:solid #4472C4 1.0pt;
  mso-border-themecolor:accent5;border-left:none;mso-border-top-alt:solid #4472C4 .5pt;
  mso-border-top-themecolor:accent5;mso-border-bottom-alt:solid #4472C4 .5pt;
  mso-border-bottom-themecolor:accent5;mso-border-right-alt:solid #4472C4 .5pt;
  mso-border-right-themecolor:accent5;background:#4472C4;mso-background-themecolor:
  accent5;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal;tab-stops:88.45pt;mso-yfti-cnfc:1'><span
  style='color:white;mso-themecolor:background1;mso-bidi-font-weight:bold'>Descripción
  <o:p></o:p></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:0'>
  <td width=124 valign=top style='width:88.25pt;border:solid #8EAADB 1.0pt;
  mso-border-themecolor:accent5;mso-border-themetint:153;border-top:none;
  mso-border-top-alt:solid #8EAADB .5pt;mso-border-top-themecolor:accent5;
  mso-border-top-themetint:153;mso-border-alt:solid #8EAADB .5pt;mso-border-themecolor:
  accent5;mso-border-themetint:153;background:#D9E2F3;mso-background-themecolor:
  accent5;mso-background-themetint:51;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;tab-stops:88.45pt;mso-yfti-cnfc:68'><span class=SpellE><span
  style='mso-bidi-font-weight:bold'>nombre_producto</span></span><span
  style='mso-bidi-font-weight:bold'><o:p></o:p></span></p>
  </td>
  <td width=107 valign=top style='width:88.25pt;border-top:none;border-left:
  none;border-bottom:solid #8EAADB 1.0pt;mso-border-bottom-themecolor:accent5;
  mso-border-bottom-themetint:153;border-right:solid #8EAADB 1.0pt;mso-border-right-themecolor:
  accent5;mso-border-right-themetint:153;mso-border-top-alt:solid #8EAADB .5pt;
  mso-border-top-themecolor:accent5;mso-border-top-themetint:153;mso-border-left-alt:
  solid #8EAADB .5pt;mso-border-left-themecolor:accent5;mso-border-left-themetint:
  153;mso-border-alt:solid #8EAADB .5pt;mso-border-themecolor:accent5;
  mso-border-themetint:153;background:#D9E2F3;mso-background-themecolor:accent5;
  mso-background-themetint:51;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;tab-stops:88.45pt;mso-yfti-cnfc:64'><span class=SpellE>String</span></p>
  </td>
  <td width=109 valign=top style='width:88.3pt;border-top:none;border-left:
  none;border-bottom:solid #8EAADB 1.0pt;mso-border-bottom-themecolor:accent5;
  mso-border-bottom-themetint:153;border-right:solid #8EAADB 1.0pt;mso-border-right-themecolor:
  accent5;mso-border-right-themetint:153;mso-border-top-alt:solid #8EAADB .5pt;
  mso-border-top-themecolor:accent5;mso-border-top-themetint:153;mso-border-left-alt:
  solid #8EAADB .5pt;mso-border-left-themecolor:accent5;mso-border-left-themetint:
  153;mso-border-alt:solid #8EAADB .5pt;mso-border-themecolor:accent5;
  mso-border-themetint:153;background:#D9E2F3;mso-background-themecolor:accent5;
  mso-background-themetint:51;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;tab-stops:88.45pt;mso-yfti-cnfc:64'>100</p>
  </td>
  <td width=111 valign=top style='width:88.3pt;border-top:none;border-left:
  none;border-bottom:solid #8EAADB 1.0pt;mso-border-bottom-themecolor:accent5;
  mso-border-bottom-themetint:153;border-right:solid #8EAADB 1.0pt;mso-border-right-themecolor:
  accent5;mso-border-right-themetint:153;mso-border-top-alt:solid #8EAADB .5pt;
  mso-border-top-themecolor:accent5;mso-border-top-themetint:153;mso-border-left-alt:
  solid #8EAADB .5pt;mso-border-left-themecolor:accent5;mso-border-left-themetint:
  153;mso-border-alt:solid #8EAADB .5pt;mso-border-themecolor:accent5;
  mso-border-themetint:153;background:#D9E2F3;mso-background-themecolor:accent5;
  mso-background-themetint:51;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;tab-stops:88.45pt;mso-yfti-cnfc:64'>Si</p>
  </td>
  <td width=115 valign=top style='width:88.3pt;border-top:none;border-left:
  none;border-bottom:solid #8EAADB 1.0pt;mso-border-bottom-themecolor:accent5;
  mso-border-bottom-themetint:153;border-right:solid #8EAADB 1.0pt;mso-border-right-themecolor:
  accent5;mso-border-right-themetint:153;mso-border-top-alt:solid #8EAADB .5pt;
  mso-border-top-themecolor:accent5;mso-border-top-themetint:153;mso-border-left-alt:
  solid #8EAADB .5pt;mso-border-left-themecolor:accent5;mso-border-left-themetint:
  153;mso-border-alt:solid #8EAADB .5pt;mso-border-themecolor:accent5;
  mso-border-themetint:153;background:#D9E2F3;mso-background-themecolor:accent5;
  mso-background-themetint:51;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;tab-stops:88.45pt;mso-yfti-cnfc:64'>Nombre del producto</p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:1'>
  <td width=124 valign=top style='width:88.25pt;border:solid #8EAADB 1.0pt;
  mso-border-themecolor:accent5;mso-border-themetint:153;border-top:none;
  mso-border-top-alt:solid #8EAADB .5pt;mso-border-top-themecolor:accent5;
  mso-border-top-themetint:153;mso-border-alt:solid #8EAADB .5pt;mso-border-themecolor:
  accent5;mso-border-themetint:153;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;tab-stops:88.45pt;mso-yfti-cnfc:4'><span style='mso-bidi-font-weight:
  bold'>precio<o:p></o:p></span></p>
  </td>
  <td width=107 valign=top style='width:88.25pt;border-top:none;border-left:
  none;border-bottom:solid #8EAADB 1.0pt;mso-border-bottom-themecolor:accent5;
  mso-border-bottom-themetint:153;border-right:solid #8EAADB 1.0pt;mso-border-right-themecolor:
  accent5;mso-border-right-themetint:153;mso-border-top-alt:solid #8EAADB .5pt;
  mso-border-top-themecolor:accent5;mso-border-top-themetint:153;mso-border-left-alt:
  solid #8EAADB .5pt;mso-border-left-themecolor:accent5;mso-border-left-themetint:
  153;mso-border-alt:solid #8EAADB .5pt;mso-border-themecolor:accent5;
  mso-border-themetint:153;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;tab-stops:88.45pt'><span class=SpellE>Double</span></p>
  </td>
  <td width=109 valign=top style='width:88.3pt;border-top:none;border-left:
  none;border-bottom:solid #8EAADB 1.0pt;mso-border-bottom-themecolor:accent5;
  mso-border-bottom-themetint:153;border-right:solid #8EAADB 1.0pt;mso-border-right-themecolor:
  accent5;mso-border-right-themetint:153;mso-border-top-alt:solid #8EAADB .5pt;
  mso-border-top-themecolor:accent5;mso-border-top-themetint:153;mso-border-left-alt:
  solid #8EAADB .5pt;mso-border-left-themecolor:accent5;mso-border-left-themetint:
  153;mso-border-alt:solid #8EAADB .5pt;mso-border-themecolor:accent5;
  mso-border-themetint:153;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;tab-stops:88.45pt'>-</p>
  </td>
  <td width=111 valign=top style='width:88.3pt;border-top:none;border-left:
  none;border-bottom:solid #8EAADB 1.0pt;mso-border-bottom-themecolor:accent5;
  mso-border-bottom-themetint:153;border-right:solid #8EAADB 1.0pt;mso-border-right-themecolor:
  accent5;mso-border-right-themetint:153;mso-border-top-alt:solid #8EAADB .5pt;
  mso-border-top-themecolor:accent5;mso-border-top-themetint:153;mso-border-left-alt:
  solid #8EAADB .5pt;mso-border-left-themecolor:accent5;mso-border-left-themetint:
  153;mso-border-alt:solid #8EAADB .5pt;mso-border-themecolor:accent5;
  mso-border-themetint:153;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;tab-stops:88.45pt'>Si</p>
  </td>
  <td width=115 valign=top style='width:88.3pt;border-top:none;border-left:
  none;border-bottom:solid #8EAADB 1.0pt;mso-border-bottom-themecolor:accent5;
  mso-border-bottom-themetint:153;border-right:solid #8EAADB 1.0pt;mso-border-right-themecolor:
  accent5;mso-border-right-themetint:153;mso-border-top-alt:solid #8EAADB .5pt;
  mso-border-top-themecolor:accent5;mso-border-top-themetint:153;mso-border-left-alt:
  solid #8EAADB .5pt;mso-border-left-themecolor:accent5;mso-border-left-themetint:
  153;mso-border-alt:solid #8EAADB .5pt;mso-border-themecolor:accent5;
  mso-border-themetint:153;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;tab-stops:88.45pt'>Precio del producto</p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:2'>
  <td width=124 valign=top style='width:88.25pt;border:solid #8EAADB 1.0pt;
  mso-border-themecolor:accent5;mso-border-themetint:153;border-top:none;
  mso-border-top-alt:solid #8EAADB .5pt;mso-border-top-themecolor:accent5;
  mso-border-top-themetint:153;mso-border-alt:solid #8EAADB .5pt;mso-border-themecolor:
  accent5;mso-border-themetint:153;background:#D9E2F3;mso-background-themecolor:
  accent5;mso-background-themetint:51;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;tab-stops:88.45pt;mso-yfti-cnfc:68'><span style='mso-bidi-font-weight:
  bold'>fotografía<o:p></o:p></span></p>
  </td>
  <td width=107 valign=top style='width:88.25pt;border-top:none;border-left:
  none;border-bottom:solid #8EAADB 1.0pt;mso-border-bottom-themecolor:accent5;
  mso-border-bottom-themetint:153;border-right:solid #8EAADB 1.0pt;mso-border-right-themecolor:
  accent5;mso-border-right-themetint:153;mso-border-top-alt:solid #8EAADB .5pt;
  mso-border-top-themecolor:accent5;mso-border-top-themetint:153;mso-border-left-alt:
  solid #8EAADB .5pt;mso-border-left-themecolor:accent5;mso-border-left-themetint:
  153;mso-border-alt:solid #8EAADB .5pt;mso-border-themecolor:accent5;
  mso-border-themetint:153;background:#D9E2F3;mso-background-themecolor:accent5;
  mso-background-themetint:51;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;tab-stops:88.45pt;mso-yfti-cnfc:64'>Blob</p>
  </td>
  <td width=109 valign=top style='width:88.3pt;border-top:none;border-left:
  none;border-bottom:solid #8EAADB 1.0pt;mso-border-bottom-themecolor:accent5;
  mso-border-bottom-themetint:153;border-right:solid #8EAADB 1.0pt;mso-border-right-themecolor:
  accent5;mso-border-right-themetint:153;mso-border-top-alt:solid #8EAADB .5pt;
  mso-border-top-themecolor:accent5;mso-border-top-themetint:153;mso-border-left-alt:
  solid #8EAADB .5pt;mso-border-left-themecolor:accent5;mso-border-left-themetint:
  153;mso-border-alt:solid #8EAADB .5pt;mso-border-themecolor:accent5;
  mso-border-themetint:153;background:#D9E2F3;mso-background-themecolor:accent5;
  mso-background-themetint:51;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;tab-stops:88.45pt;mso-yfti-cnfc:64'>-</p>
  </td>
  <td width=111 valign=top style='width:88.3pt;border-top:none;border-left:
  none;border-bottom:solid #8EAADB 1.0pt;mso-border-bottom-themecolor:accent5;
  mso-border-bottom-themetint:153;border-right:solid #8EAADB 1.0pt;mso-border-right-themecolor:
  accent5;mso-border-right-themetint:153;mso-border-top-alt:solid #8EAADB .5pt;
  mso-border-top-themecolor:accent5;mso-border-top-themetint:153;mso-border-left-alt:
  solid #8EAADB .5pt;mso-border-left-themecolor:accent5;mso-border-left-themetint:
  153;mso-border-alt:solid #8EAADB .5pt;mso-border-themecolor:accent5;
  mso-border-themetint:153;background:#D9E2F3;mso-background-themecolor:accent5;
  mso-background-themetint:51;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;tab-stops:88.45pt;mso-yfti-cnfc:64'>Si</p>
  </td>
  <td width=115 valign=top style='width:88.3pt;border-top:none;border-left:
  none;border-bottom:solid #8EAADB 1.0pt;mso-border-bottom-themecolor:accent5;
  mso-border-bottom-themetint:153;border-right:solid #8EAADB 1.0pt;mso-border-right-themecolor:
  accent5;mso-border-right-themetint:153;mso-border-top-alt:solid #8EAADB .5pt;
  mso-border-top-themecolor:accent5;mso-border-top-themetint:153;mso-border-left-alt:
  solid #8EAADB .5pt;mso-border-left-themecolor:accent5;mso-border-left-themetint:
  153;mso-border-alt:solid #8EAADB .5pt;mso-border-themecolor:accent5;
  mso-border-themetint:153;background:#D9E2F3;mso-background-themecolor:accent5;
  mso-background-themetint:51;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;tab-stops:88.45pt;mso-yfti-cnfc:64'>Fotografía del producto en base 64</p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:3'>
  <td width=124 valign=top style='width:88.25pt;border:solid #8EAADB 1.0pt;
  mso-border-themecolor:accent5;mso-border-themetint:153;border-top:none;
  mso-border-top-alt:solid #8EAADB .5pt;mso-border-top-themecolor:accent5;
  mso-border-top-themetint:153;mso-border-alt:solid #8EAADB .5pt;mso-border-themecolor:
  accent5;mso-border-themetint:153;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;tab-stops:88.45pt;mso-yfti-cnfc:4'><span style='mso-bidi-font-weight:
  bold'>existencia<o:p></o:p></span></p>
  </td>
  <td width=107 valign=top style='width:88.25pt;border-top:none;border-left:
  none;border-bottom:solid #8EAADB 1.0pt;mso-border-bottom-themecolor:accent5;
  mso-border-bottom-themetint:153;border-right:solid #8EAADB 1.0pt;mso-border-right-themecolor:
  accent5;mso-border-right-themetint:153;mso-border-top-alt:solid #8EAADB .5pt;
  mso-border-top-themecolor:accent5;mso-border-top-themetint:153;mso-border-left-alt:
  solid #8EAADB .5pt;mso-border-left-themecolor:accent5;mso-border-left-themetint:
  153;mso-border-alt:solid #8EAADB .5pt;mso-border-themecolor:accent5;
  mso-border-themetint:153;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;tab-stops:88.45pt'><span class=SpellE>Int</span></p>
  </td>
  <td width=109 valign=top style='width:88.3pt;border-top:none;border-left:
  none;border-bottom:solid #8EAADB 1.0pt;mso-border-bottom-themecolor:accent5;
  mso-border-bottom-themetint:153;border-right:solid #8EAADB 1.0pt;mso-border-right-themecolor:
  accent5;mso-border-right-themetint:153;mso-border-top-alt:solid #8EAADB .5pt;
  mso-border-top-themecolor:accent5;mso-border-top-themetint:153;mso-border-left-alt:
  solid #8EAADB .5pt;mso-border-left-themecolor:accent5;mso-border-left-themetint:
  153;mso-border-alt:solid #8EAADB .5pt;mso-border-themecolor:accent5;
  mso-border-themetint:153;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;tab-stops:88.45pt'>-</p>
  </td>
  <td width=111 valign=top style='width:88.3pt;border-top:none;border-left:
  none;border-bottom:solid #8EAADB 1.0pt;mso-border-bottom-themecolor:accent5;
  mso-border-bottom-themetint:153;border-right:solid #8EAADB 1.0pt;mso-border-right-themecolor:
  accent5;mso-border-right-themetint:153;mso-border-top-alt:solid #8EAADB .5pt;
  mso-border-top-themecolor:accent5;mso-border-top-themetint:153;mso-border-left-alt:
  solid #8EAADB .5pt;mso-border-left-themecolor:accent5;mso-border-left-themetint:
  153;mso-border-alt:solid #8EAADB .5pt;mso-border-themecolor:accent5;
  mso-border-themetint:153;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;tab-stops:88.45pt'>Si</p>
  </td>
  <td width=115 valign=top style='width:88.3pt;border-top:none;border-left:
  none;border-bottom:solid #8EAADB 1.0pt;mso-border-bottom-themecolor:accent5;
  mso-border-bottom-themetint:153;border-right:solid #8EAADB 1.0pt;mso-border-right-themecolor:
  accent5;mso-border-right-themetint:153;mso-border-top-alt:solid #8EAADB .5pt;
  mso-border-top-themecolor:accent5;mso-border-top-themetint:153;mso-border-left-alt:
  solid #8EAADB .5pt;mso-border-left-themecolor:accent5;mso-border-left-themetint:
  153;mso-border-alt:solid #8EAADB .5pt;mso-border-themecolor:accent5;
  mso-border-themetint:153;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;tab-stops:88.45pt'>Disponibilidad del producto</p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:4;mso-yfti-lastrow:yes'>
  <td width=124 valign=top style='width:88.25pt;border:solid #8EAADB 1.0pt;
  mso-border-themecolor:accent5;mso-border-themetint:153;border-top:none;
  mso-border-top-alt:solid #8EAADB .5pt;mso-border-top-themecolor:accent5;
  mso-border-top-themetint:153;mso-border-alt:solid #8EAADB .5pt;mso-border-themecolor:
  accent5;mso-border-themetint:153;background:#D9E2F3;mso-background-themecolor:
  accent5;mso-background-themetint:51;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;tab-stops:88.45pt;mso-yfti-cnfc:68'><span class=SpellE><span
  style='mso-bidi-font-weight:bold'>tipo_producto</span></span><span
  style='mso-bidi-font-weight:bold'><o:p></o:p></span></p>
  </td>
  <td width=107 valign=top style='width:88.25pt;border-top:none;border-left:
  none;border-bottom:solid #8EAADB 1.0pt;mso-border-bottom-themecolor:accent5;
  mso-border-bottom-themetint:153;border-right:solid #8EAADB 1.0pt;mso-border-right-themecolor:
  accent5;mso-border-right-themetint:153;mso-border-top-alt:solid #8EAADB .5pt;
  mso-border-top-themecolor:accent5;mso-border-top-themetint:153;mso-border-left-alt:
  solid #8EAADB .5pt;mso-border-left-themecolor:accent5;mso-border-left-themetint:
  153;mso-border-alt:solid #8EAADB .5pt;mso-border-themecolor:accent5;
  mso-border-themetint:153;background:#D9E2F3;mso-background-themecolor:accent5;
  mso-background-themetint:51;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;tab-stops:88.45pt;mso-yfti-cnfc:64'><span class=SpellE>int</span></p>
  </td>
  <td width=109 valign=top style='width:88.3pt;border-top:none;border-left:
  none;border-bottom:solid #8EAADB 1.0pt;mso-border-bottom-themecolor:accent5;
  mso-border-bottom-themetint:153;border-right:solid #8EAADB 1.0pt;mso-border-right-themecolor:
  accent5;mso-border-right-themetint:153;mso-border-top-alt:solid #8EAADB .5pt;
  mso-border-top-themecolor:accent5;mso-border-top-themetint:153;mso-border-left-alt:
  solid #8EAADB .5pt;mso-border-left-themecolor:accent5;mso-border-left-themetint:
  153;mso-border-alt:solid #8EAADB .5pt;mso-border-themecolor:accent5;
  mso-border-themetint:153;background:#D9E2F3;mso-background-themecolor:accent5;
  mso-background-themetint:51;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;tab-stops:88.45pt;mso-yfti-cnfc:64'>-</p>
  </td>
  <td width=111 valign=top style='width:88.3pt;border-top:none;border-left:
  none;border-bottom:solid #8EAADB 1.0pt;mso-border-bottom-themecolor:accent5;
  mso-border-bottom-themetint:153;border-right:solid #8EAADB 1.0pt;mso-border-right-themecolor:
  accent5;mso-border-right-themetint:153;mso-border-top-alt:solid #8EAADB .5pt;
  mso-border-top-themecolor:accent5;mso-border-top-themetint:153;mso-border-left-alt:
  solid #8EAADB .5pt;mso-border-left-themecolor:accent5;mso-border-left-themetint:
  153;mso-border-alt:solid #8EAADB .5pt;mso-border-themecolor:accent5;
  mso-border-themetint:153;background:#D9E2F3;mso-background-themecolor:accent5;
  mso-background-themetint:51;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;tab-stops:88.45pt;mso-yfti-cnfc:64'>Si </p>
  </td>
  <td width=115 valign=top style='width:88.3pt;border-top:none;border-left:
  none;border-bottom:solid #8EAADB 1.0pt;mso-border-bottom-themecolor:accent5;
  mso-border-bottom-themetint:153;border-right:solid #8EAADB 1.0pt;mso-border-right-themecolor:
  accent5;mso-border-right-themetint:153;mso-border-top-alt:solid #8EAADB .5pt;
  mso-border-top-themecolor:accent5;mso-border-top-themetint:153;mso-border-left-alt:
  solid #8EAADB .5pt;mso-border-left-themecolor:accent5;mso-border-left-themetint:
  153;mso-border-alt:solid #8EAADB .5pt;mso-border-themecolor:accent5;
  mso-border-themetint:153;background:#D9E2F3;mso-background-themecolor:accent5;
  mso-background-themetint:51;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;tab-stops:88.45pt;mso-yfti-cnfc:64'>Identificador del tipo de producto</p>
  </td>
 </tr>
</table>

Ejemplo de uso

Entrada:

url: api/productos

JSON:

![enter image description here](https://i.gyazo.com/b0e459fef5be516e06785b0376d56c41.png)

Salidas:

En caso de éxito:

Http Status: 201

En caso de éxito:

![enter image description here](https://i.gyazo.com/837c9c9a8b5331ff49e52f06fe803781.png)


En caso de fallo:

HTTP Status: 500

## Servicio PUT

**Nombre del servicio:** Productos

**Nombre del método:** productos

**URL:** /api/productos

**Tipo de HTTP Method:** PUT

**Definición de atributos de JSON de entrada:**

<table class=MsoTable15Grid4Accent5 border=1 cellspacing=0 cellpadding=0
 style='border-collapse:collapse;border:none;mso-border-alt:solid #8EAADB .5pt;
 mso-border-themecolor:accent5;mso-border-themetint:153;mso-yfti-tbllook:1184;
 mso-padding-alt:0cm 5.4pt 0cm 5.4pt'>
 <tr style='mso-yfti-irow:-1;mso-yfti-firstrow:yes;mso-yfti-lastfirstrow:yes'>
  <td width=113 valign=top style='width:88.25pt;border:solid #4472C4 1.0pt;
  mso-border-themecolor:accent5;border-right:none;mso-border-top-alt:solid #4472C4 .5pt;
  mso-border-top-themecolor:accent5;mso-border-left-alt:solid #4472C4 .5pt;
  mso-border-left-themecolor:accent5;mso-border-bottom-alt:solid #4472C4 .5pt;
  mso-border-bottom-themecolor:accent5;background:#4472C4;mso-background-themecolor:
  accent5;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal;tab-stops:88.45pt;mso-yfti-cnfc:5'><b><span
  style='color:white;mso-themecolor:background1'>Atributo<o:p></o:p></span></b></p>
  </td>
  <td width=111 valign=top style='width:88.25pt;border-top:solid #4472C4 1.0pt;
  mso-border-top-themecolor:accent5;border-left:none;border-bottom:solid #4472C4 1.0pt;
  mso-border-bottom-themecolor:accent5;border-right:none;mso-border-top-alt:
  solid #4472C4 .5pt;mso-border-top-themecolor:accent5;mso-border-bottom-alt:
  solid #4472C4 .5pt;mso-border-bottom-themecolor:accent5;background:#4472C4;
  mso-background-themecolor:accent5;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal;tab-stops:88.45pt;mso-yfti-cnfc:1'><b><span
  style='color:white;mso-themecolor:background1'>Tipo de dato<o:p></o:p></span></b></p>
  </td>
  <td width=112 valign=top style='width:88.3pt;border-top:solid #4472C4 1.0pt;
  mso-border-top-themecolor:accent5;border-left:none;border-bottom:solid #4472C4 1.0pt;
  mso-border-bottom-themecolor:accent5;border-right:none;mso-border-top-alt:
  solid #4472C4 .5pt;mso-border-top-themecolor:accent5;mso-border-bottom-alt:
  solid #4472C4 .5pt;mso-border-bottom-themecolor:accent5;background:#4472C4;
  mso-background-themecolor:accent5;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal;tab-stops:88.45pt;mso-yfti-cnfc:1'><b><span
  style='color:white;mso-themecolor:background1'>Longitud <span class=SpellE>maxima</span><o:p></o:p></span></b></p>
  </td>
  <td width=114 valign=top style='width:88.3pt;border-top:solid #4472C4 1.0pt;
  mso-border-top-themecolor:accent5;border-left:none;border-bottom:solid #4472C4 1.0pt;
  mso-border-bottom-themecolor:accent5;border-right:none;mso-border-top-alt:
  solid #4472C4 .5pt;mso-border-top-themecolor:accent5;mso-border-bottom-alt:
  solid #4472C4 .5pt;mso-border-bottom-themecolor:accent5;background:#4472C4;
  mso-background-themecolor:accent5;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal;tab-stops:88.45pt;mso-yfti-cnfc:1'><b><span
  style='color:white;mso-themecolor:background1'>obligatorio<o:p></o:p></span></b></p>
  </td>
  <td width=116 valign=top style='width:88.3pt;border:solid #4472C4 1.0pt;
  mso-border-themecolor:accent5;border-left:none;mso-border-top-alt:solid #4472C4 .5pt;
  mso-border-top-themecolor:accent5;mso-border-bottom-alt:solid #4472C4 .5pt;
  mso-border-bottom-themecolor:accent5;mso-border-right-alt:solid #4472C4 .5pt;
  mso-border-right-themecolor:accent5;background:#4472C4;mso-background-themecolor:
  accent5;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal;tab-stops:88.45pt;mso-yfti-cnfc:1'><b><span
  style='color:white;mso-themecolor:background1'>Descripción <o:p></o:p></span></b></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:0'>
  <td width=113 valign=top style='width:88.25pt;border:solid #8EAADB 1.0pt;
  mso-border-themecolor:accent5;mso-border-themetint:153;border-top:none;
  mso-border-top-alt:solid #8EAADB .5pt;mso-border-top-themecolor:accent5;
  mso-border-top-themetint:153;mso-border-alt:solid #8EAADB .5pt;mso-border-themecolor:
  accent5;mso-border-themetint:153;background:#D9E2F3;mso-background-themecolor:
  accent5;mso-background-themetint:51;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;tab-stops:88.45pt;mso-yfti-cnfc:68'><span style='mso-bidi-font-weight:
  bold'>nombre<o:p></o:p></span></p>
  </td>
  <td width=111 valign=top style='width:88.25pt;border-top:none;border-left:
  none;border-bottom:solid #8EAADB 1.0pt;mso-border-bottom-themecolor:accent5;
  mso-border-bottom-themetint:153;border-right:solid #8EAADB 1.0pt;mso-border-right-themecolor:
  accent5;mso-border-right-themetint:153;mso-border-top-alt:solid #8EAADB .5pt;
  mso-border-top-themecolor:accent5;mso-border-top-themetint:153;mso-border-left-alt:
  solid #8EAADB .5pt;mso-border-left-themecolor:accent5;mso-border-left-themetint:
  153;mso-border-alt:solid #8EAADB .5pt;mso-border-themecolor:accent5;
  mso-border-themetint:153;background:#D9E2F3;mso-background-themecolor:accent5;
  mso-background-themetint:51;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;tab-stops:88.45pt;mso-yfti-cnfc:64'><span class=SpellE>string</span></p>
  </td>
  <td width=112 valign=top style='width:88.3pt;border-top:none;border-left:
  none;border-bottom:solid #8EAADB 1.0pt;mso-border-bottom-themecolor:accent5;
  mso-border-bottom-themetint:153;border-right:solid #8EAADB 1.0pt;mso-border-right-themecolor:
  accent5;mso-border-right-themetint:153;mso-border-top-alt:solid #8EAADB .5pt;
  mso-border-top-themecolor:accent5;mso-border-top-themetint:153;mso-border-left-alt:
  solid #8EAADB .5pt;mso-border-left-themecolor:accent5;mso-border-left-themetint:
  153;mso-border-alt:solid #8EAADB .5pt;mso-border-themecolor:accent5;
  mso-border-themetint:153;background:#D9E2F3;mso-background-themecolor:accent5;
  mso-background-themetint:51;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;tab-stops:88.45pt;mso-yfti-cnfc:64'>100</p>
  </td>
  <td width=114 valign=top style='width:88.3pt;border-top:none;border-left:
  none;border-bottom:solid #8EAADB 1.0pt;mso-border-bottom-themecolor:accent5;
  mso-border-bottom-themetint:153;border-right:solid #8EAADB 1.0pt;mso-border-right-themecolor:
  accent5;mso-border-right-themetint:153;mso-border-top-alt:solid #8EAADB .5pt;
  mso-border-top-themecolor:accent5;mso-border-top-themetint:153;mso-border-left-alt:
  solid #8EAADB .5pt;mso-border-left-themecolor:accent5;mso-border-left-themetint:
  153;mso-border-alt:solid #8EAADB .5pt;mso-border-themecolor:accent5;
  mso-border-themetint:153;background:#D9E2F3;mso-background-themecolor:accent5;
  mso-background-themetint:51;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;tab-stops:88.45pt;mso-yfti-cnfc:64'>Si</p>
  </td>
  <td width=116 valign=top style='width:88.3pt;border-top:none;border-left:
  none;border-bottom:solid #8EAADB 1.0pt;mso-border-bottom-themecolor:accent5;
  mso-border-bottom-themetint:153;border-right:solid #8EAADB 1.0pt;mso-border-right-themecolor:
  accent5;mso-border-right-themetint:153;mso-border-top-alt:solid #8EAADB .5pt;
  mso-border-top-themecolor:accent5;mso-border-top-themetint:153;mso-border-left-alt:
  solid #8EAADB .5pt;mso-border-left-themecolor:accent5;mso-border-left-themetint:
  153;mso-border-alt:solid #8EAADB .5pt;mso-border-themecolor:accent5;
  mso-border-themetint:153;background:#D9E2F3;mso-background-themecolor:accent5;
  mso-background-themetint:51;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;tab-stops:88.45pt;mso-yfti-cnfc:64'>Nombre del producto</p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:1'>
  <td width=113 valign=top style='width:88.25pt;border:solid #8EAADB 1.0pt;
  mso-border-themecolor:accent5;mso-border-themetint:153;border-top:none;
  mso-border-top-alt:solid #8EAADB .5pt;mso-border-top-themecolor:accent5;
  mso-border-top-themetint:153;mso-border-alt:solid #8EAADB .5pt;mso-border-themecolor:
  accent5;mso-border-themetint:153;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;tab-stops:88.45pt;mso-yfti-cnfc:4'><span style='mso-bidi-font-weight:
  bold'>precio<o:p></o:p></span></p>
  </td>
  <td width=111 valign=top style='width:88.25pt;border-top:none;border-left:
  none;border-bottom:solid #8EAADB 1.0pt;mso-border-bottom-themecolor:accent5;
  mso-border-bottom-themetint:153;border-right:solid #8EAADB 1.0pt;mso-border-right-themecolor:
  accent5;mso-border-right-themetint:153;mso-border-top-alt:solid #8EAADB .5pt;
  mso-border-top-themecolor:accent5;mso-border-top-themetint:153;mso-border-left-alt:
  solid #8EAADB .5pt;mso-border-left-themecolor:accent5;mso-border-left-themetint:
  153;mso-border-alt:solid #8EAADB .5pt;mso-border-themecolor:accent5;
  mso-border-themetint:153;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;tab-stops:88.45pt'>Doublé</p>
  </td>
  <td width=112 valign=top style='width:88.3pt;border-top:none;border-left:
  none;border-bottom:solid #8EAADB 1.0pt;mso-border-bottom-themecolor:accent5;
  mso-border-bottom-themetint:153;border-right:solid #8EAADB 1.0pt;mso-border-right-themecolor:
  accent5;mso-border-right-themetint:153;mso-border-top-alt:solid #8EAADB .5pt;
  mso-border-top-themecolor:accent5;mso-border-top-themetint:153;mso-border-left-alt:
  solid #8EAADB .5pt;mso-border-left-themecolor:accent5;mso-border-left-themetint:
  153;mso-border-alt:solid #8EAADB .5pt;mso-border-themecolor:accent5;
  mso-border-themetint:153;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;tab-stops:88.45pt'>-</p>
  </td>
  <td width=114 valign=top style='width:88.3pt;border-top:none;border-left:
  none;border-bottom:solid #8EAADB 1.0pt;mso-border-bottom-themecolor:accent5;
  mso-border-bottom-themetint:153;border-right:solid #8EAADB 1.0pt;mso-border-right-themecolor:
  accent5;mso-border-right-themetint:153;mso-border-top-alt:solid #8EAADB .5pt;
  mso-border-top-themecolor:accent5;mso-border-top-themetint:153;mso-border-left-alt:
  solid #8EAADB .5pt;mso-border-left-themecolor:accent5;mso-border-left-themetint:
  153;mso-border-alt:solid #8EAADB .5pt;mso-border-themecolor:accent5;
  mso-border-themetint:153;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;tab-stops:88.45pt'>Si</p>
  </td>
  <td width=116 valign=top style='width:88.3pt;border-top:none;border-left:
  none;border-bottom:solid #8EAADB 1.0pt;mso-border-bottom-themecolor:accent5;
  mso-border-bottom-themetint:153;border-right:solid #8EAADB 1.0pt;mso-border-right-themecolor:
  accent5;mso-border-right-themetint:153;mso-border-top-alt:solid #8EAADB .5pt;
  mso-border-top-themecolor:accent5;mso-border-top-themetint:153;mso-border-left-alt:
  solid #8EAADB .5pt;mso-border-left-themecolor:accent5;mso-border-left-themetint:
  153;mso-border-alt:solid #8EAADB .5pt;mso-border-themecolor:accent5;
  mso-border-themetint:153;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;tab-stops:88.45pt'>Precio del producto</p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:2'>
  <td width=113 valign=top style='width:88.25pt;border:solid #8EAADB 1.0pt;
  mso-border-themecolor:accent5;mso-border-themetint:153;border-top:none;
  mso-border-top-alt:solid #8EAADB .5pt;mso-border-top-themecolor:accent5;
  mso-border-top-themetint:153;mso-border-alt:solid #8EAADB .5pt;mso-border-themecolor:
  accent5;mso-border-themetint:153;background:#D9E2F3;mso-background-themecolor:
  accent5;mso-background-themetint:51;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;tab-stops:88.45pt;mso-yfti-cnfc:68'><span style='mso-bidi-font-weight:
  bold'>foto<o:p></o:p></span></p>
  </td>
  <td width=111 valign=top style='width:88.25pt;border-top:none;border-left:
  none;border-bottom:solid #8EAADB 1.0pt;mso-border-bottom-themecolor:accent5;
  mso-border-bottom-themetint:153;border-right:solid #8EAADB 1.0pt;mso-border-right-themecolor:
  accent5;mso-border-right-themetint:153;mso-border-top-alt:solid #8EAADB .5pt;
  mso-border-top-themecolor:accent5;mso-border-top-themetint:153;mso-border-left-alt:
  solid #8EAADB .5pt;mso-border-left-themecolor:accent5;mso-border-left-themetint:
  153;mso-border-alt:solid #8EAADB .5pt;mso-border-themecolor:accent5;
  mso-border-themetint:153;background:#D9E2F3;mso-background-themecolor:accent5;
  mso-background-themetint:51;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;tab-stops:88.45pt;mso-yfti-cnfc:64'>Blob</p>
  </td>
  <td width=112 valign=top style='width:88.3pt;border-top:none;border-left:
  none;border-bottom:solid #8EAADB 1.0pt;mso-border-bottom-themecolor:accent5;
  mso-border-bottom-themetint:153;border-right:solid #8EAADB 1.0pt;mso-border-right-themecolor:
  accent5;mso-border-right-themetint:153;mso-border-top-alt:solid #8EAADB .5pt;
  mso-border-top-themecolor:accent5;mso-border-top-themetint:153;mso-border-left-alt:
  solid #8EAADB .5pt;mso-border-left-themecolor:accent5;mso-border-left-themetint:
  153;mso-border-alt:solid #8EAADB .5pt;mso-border-themecolor:accent5;
  mso-border-themetint:153;background:#D9E2F3;mso-background-themecolor:accent5;
  mso-background-themetint:51;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;tab-stops:88.45pt;mso-yfti-cnfc:64'>-</p>
  </td>
  <td width=114 valign=top style='width:88.3pt;border-top:none;border-left:
  none;border-bottom:solid #8EAADB 1.0pt;mso-border-bottom-themecolor:accent5;
  mso-border-bottom-themetint:153;border-right:solid #8EAADB 1.0pt;mso-border-right-themecolor:
  accent5;mso-border-right-themetint:153;mso-border-top-alt:solid #8EAADB .5pt;
  mso-border-top-themecolor:accent5;mso-border-top-themetint:153;mso-border-left-alt:
  solid #8EAADB .5pt;mso-border-left-themecolor:accent5;mso-border-left-themetint:
  153;mso-border-alt:solid #8EAADB .5pt;mso-border-themecolor:accent5;
  mso-border-themetint:153;background:#D9E2F3;mso-background-themecolor:accent5;
  mso-background-themetint:51;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;tab-stops:88.45pt;mso-yfti-cnfc:64'>Si</p>
  </td>
  <td width=116 valign=top style='width:88.3pt;border-top:none;border-left:
  none;border-bottom:solid #8EAADB 1.0pt;mso-border-bottom-themecolor:accent5;
  mso-border-bottom-themetint:153;border-right:solid #8EAADB 1.0pt;mso-border-right-themecolor:
  accent5;mso-border-right-themetint:153;mso-border-top-alt:solid #8EAADB .5pt;
  mso-border-top-themecolor:accent5;mso-border-top-themetint:153;mso-border-left-alt:
  solid #8EAADB .5pt;mso-border-left-themecolor:accent5;mso-border-left-themetint:
  153;mso-border-alt:solid #8EAADB .5pt;mso-border-themecolor:accent5;
  mso-border-themetint:153;background:#D9E2F3;mso-background-themecolor:accent5;
  mso-background-themetint:51;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;tab-stops:88.45pt;mso-yfti-cnfc:64'>Fotografía del producto en base 64</p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:3'>
  <td width=113 valign=top style='width:88.25pt;border:solid #8EAADB 1.0pt;
  mso-border-themecolor:accent5;mso-border-themetint:153;border-top:none;
  mso-border-top-alt:solid #8EAADB .5pt;mso-border-top-themecolor:accent5;
  mso-border-top-themetint:153;mso-border-alt:solid #8EAADB .5pt;mso-border-themecolor:
  accent5;mso-border-themetint:153;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;tab-stops:88.45pt;mso-yfti-cnfc:4'><span style='mso-bidi-font-weight:
  bold'>existencia<o:p></o:p></span></p>
  </td>
  <td width=111 valign=top style='width:88.25pt;border-top:none;border-left:
  none;border-bottom:solid #8EAADB 1.0pt;mso-border-bottom-themecolor:accent5;
  mso-border-bottom-themetint:153;border-right:solid #8EAADB 1.0pt;mso-border-right-themecolor:
  accent5;mso-border-right-themetint:153;mso-border-top-alt:solid #8EAADB .5pt;
  mso-border-top-themecolor:accent5;mso-border-top-themetint:153;mso-border-left-alt:
  solid #8EAADB .5pt;mso-border-left-themecolor:accent5;mso-border-left-themetint:
  153;mso-border-alt:solid #8EAADB .5pt;mso-border-themecolor:accent5;
  mso-border-themetint:153;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;tab-stops:88.45pt'><span class=SpellE>Int</span></p>
  </td>
  <td width=112 valign=top style='width:88.3pt;border-top:none;border-left:
  none;border-bottom:solid #8EAADB 1.0pt;mso-border-bottom-themecolor:accent5;
  mso-border-bottom-themetint:153;border-right:solid #8EAADB 1.0pt;mso-border-right-themecolor:
  accent5;mso-border-right-themetint:153;mso-border-top-alt:solid #8EAADB .5pt;
  mso-border-top-themecolor:accent5;mso-border-top-themetint:153;mso-border-left-alt:
  solid #8EAADB .5pt;mso-border-left-themecolor:accent5;mso-border-left-themetint:
  153;mso-border-alt:solid #8EAADB .5pt;mso-border-themecolor:accent5;
  mso-border-themetint:153;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;tab-stops:88.45pt'>-</p>
  </td>
  <td width=114 valign=top style='width:88.3pt;border-top:none;border-left:
  none;border-bottom:solid #8EAADB 1.0pt;mso-border-bottom-themecolor:accent5;
  mso-border-bottom-themetint:153;border-right:solid #8EAADB 1.0pt;mso-border-right-themecolor:
  accent5;mso-border-right-themetint:153;mso-border-top-alt:solid #8EAADB .5pt;
  mso-border-top-themecolor:accent5;mso-border-top-themetint:153;mso-border-left-alt:
  solid #8EAADB .5pt;mso-border-left-themecolor:accent5;mso-border-left-themetint:
  153;mso-border-alt:solid #8EAADB .5pt;mso-border-themecolor:accent5;
  mso-border-themetint:153;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;tab-stops:88.45pt'>Si</p>
  </td>
  <td width=116 valign=top style='width:88.3pt;border-top:none;border-left:
  none;border-bottom:solid #8EAADB 1.0pt;mso-border-bottom-themecolor:accent5;
  mso-border-bottom-themetint:153;border-right:solid #8EAADB 1.0pt;mso-border-right-themecolor:
  accent5;mso-border-right-themetint:153;mso-border-top-alt:solid #8EAADB .5pt;
  mso-border-top-themecolor:accent5;mso-border-top-themetint:153;mso-border-left-alt:
  solid #8EAADB .5pt;mso-border-left-themecolor:accent5;mso-border-left-themetint:
  153;mso-border-alt:solid #8EAADB .5pt;mso-border-themecolor:accent5;
  mso-border-themetint:153;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;tab-stops:88.45pt'>Disponibilidad del producto</p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:4'>
  <td width=113 valign=top style='width:88.25pt;border:solid #8EAADB 1.0pt;
  mso-border-themecolor:accent5;mso-border-themetint:153;border-top:none;
  mso-border-top-alt:solid #8EAADB .5pt;mso-border-top-themecolor:accent5;
  mso-border-top-themetint:153;mso-border-alt:solid #8EAADB .5pt;mso-border-themecolor:
  accent5;mso-border-themetint:153;background:#D9E2F3;mso-background-themecolor:
  accent5;mso-background-themetint:51;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;tab-stops:88.45pt;mso-yfti-cnfc:68'><span style='mso-bidi-font-weight:
  bold'>tipo<o:p></o:p></span></p>
  </td>
  <td width=111 valign=top style='width:88.25pt;border-top:none;border-left:
  none;border-bottom:solid #8EAADB 1.0pt;mso-border-bottom-themecolor:accent5;
  mso-border-bottom-themetint:153;border-right:solid #8EAADB 1.0pt;mso-border-right-themecolor:
  accent5;mso-border-right-themetint:153;mso-border-top-alt:solid #8EAADB .5pt;
  mso-border-top-themecolor:accent5;mso-border-top-themetint:153;mso-border-left-alt:
  solid #8EAADB .5pt;mso-border-left-themecolor:accent5;mso-border-left-themetint:
  153;mso-border-alt:solid #8EAADB .5pt;mso-border-themecolor:accent5;
  mso-border-themetint:153;background:#D9E2F3;mso-background-themecolor:accent5;
  mso-background-themetint:51;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;tab-stops:88.45pt;mso-yfti-cnfc:64'><span class=SpellE>Int</span></p>
  </td>
  <td width=112 valign=top style='width:88.3pt;border-top:none;border-left:
  none;border-bottom:solid #8EAADB 1.0pt;mso-border-bottom-themecolor:accent5;
  mso-border-bottom-themetint:153;border-right:solid #8EAADB 1.0pt;mso-border-right-themecolor:
  accent5;mso-border-right-themetint:153;mso-border-top-alt:solid #8EAADB .5pt;
  mso-border-top-themecolor:accent5;mso-border-top-themetint:153;mso-border-left-alt:
  solid #8EAADB .5pt;mso-border-left-themecolor:accent5;mso-border-left-themetint:
  153;mso-border-alt:solid #8EAADB .5pt;mso-border-themecolor:accent5;
  mso-border-themetint:153;background:#D9E2F3;mso-background-themecolor:accent5;
  mso-background-themetint:51;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;tab-stops:88.45pt;mso-yfti-cnfc:64'>-</p>
  </td>
  <td width=114 valign=top style='width:88.3pt;border-top:none;border-left:
  none;border-bottom:solid #8EAADB 1.0pt;mso-border-bottom-themecolor:accent5;
  mso-border-bottom-themetint:153;border-right:solid #8EAADB 1.0pt;mso-border-right-themecolor:
  accent5;mso-border-right-themetint:153;mso-border-top-alt:solid #8EAADB .5pt;
  mso-border-top-themecolor:accent5;mso-border-top-themetint:153;mso-border-left-alt:
  solid #8EAADB .5pt;mso-border-left-themecolor:accent5;mso-border-left-themetint:
  153;mso-border-alt:solid #8EAADB .5pt;mso-border-themecolor:accent5;
  mso-border-themetint:153;background:#D9E2F3;mso-background-themecolor:accent5;
  mso-background-themetint:51;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;tab-stops:88.45pt;mso-yfti-cnfc:64'>Si</p>
  </td>
  <td width=116 valign=top style='width:88.3pt;border-top:none;border-left:
  none;border-bottom:solid #8EAADB 1.0pt;mso-border-bottom-themecolor:accent5;
  mso-border-bottom-themetint:153;border-right:solid #8EAADB 1.0pt;mso-border-right-themecolor:
  accent5;mso-border-right-themetint:153;mso-border-top-alt:solid #8EAADB .5pt;
  mso-border-top-themecolor:accent5;mso-border-top-themetint:153;mso-border-left-alt:
  solid #8EAADB .5pt;mso-border-left-themecolor:accent5;mso-border-left-themetint:
  153;mso-border-alt:solid #8EAADB .5pt;mso-border-themecolor:accent5;
  mso-border-themetint:153;background:#D9E2F3;mso-background-themecolor:accent5;
  mso-background-themetint:51;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;tab-stops:88.45pt;mso-yfti-cnfc:64'>Id del tipo de producto</p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:5;mso-yfti-lastrow:yes'>
  <td width=113 valign=top style='width:88.25pt;border:solid #8EAADB 1.0pt;
  mso-border-themecolor:accent5;mso-border-themetint:153;border-top:none;
  mso-border-top-alt:solid #8EAADB .5pt;mso-border-top-themecolor:accent5;
  mso-border-top-themetint:153;mso-border-alt:solid #8EAADB .5pt;mso-border-themecolor:
  accent5;mso-border-themetint:153;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;tab-stops:88.45pt;mso-yfti-cnfc:4'><span style='mso-bidi-font-weight:
  bold'>id<o:p></o:p></span></p>
  </td>
  <td width=111 valign=top style='width:88.25pt;border-top:none;border-left:
  none;border-bottom:solid #8EAADB 1.0pt;mso-border-bottom-themecolor:accent5;
  mso-border-bottom-themetint:153;border-right:solid #8EAADB 1.0pt;mso-border-right-themecolor:
  accent5;mso-border-right-themetint:153;mso-border-top-alt:solid #8EAADB .5pt;
  mso-border-top-themecolor:accent5;mso-border-top-themetint:153;mso-border-left-alt:
  solid #8EAADB .5pt;mso-border-left-themecolor:accent5;mso-border-left-themetint:
  153;mso-border-alt:solid #8EAADB .5pt;mso-border-themecolor:accent5;
  mso-border-themetint:153;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;tab-stops:88.45pt'><span class=SpellE>int</span></p>
  </td>
  <td width=112 valign=top style='width:88.3pt;border-top:none;border-left:
  none;border-bottom:solid #8EAADB 1.0pt;mso-border-bottom-themecolor:accent5;
  mso-border-bottom-themetint:153;border-right:solid #8EAADB 1.0pt;mso-border-right-themecolor:
  accent5;mso-border-right-themetint:153;mso-border-top-alt:solid #8EAADB .5pt;
  mso-border-top-themecolor:accent5;mso-border-top-themetint:153;mso-border-left-alt:
  solid #8EAADB .5pt;mso-border-left-themecolor:accent5;mso-border-left-themetint:
  153;mso-border-alt:solid #8EAADB .5pt;mso-border-themecolor:accent5;
  mso-border-themetint:153;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;tab-stops:88.45pt'>-</p>
  </td>
  <td width=114 valign=top style='width:88.3pt;border-top:none;border-left:
  none;border-bottom:solid #8EAADB 1.0pt;mso-border-bottom-themecolor:accent5;
  mso-border-bottom-themetint:153;border-right:solid #8EAADB 1.0pt;mso-border-right-themecolor:
  accent5;mso-border-right-themetint:153;mso-border-top-alt:solid #8EAADB .5pt;
  mso-border-top-themecolor:accent5;mso-border-top-themetint:153;mso-border-left-alt:
  solid #8EAADB .5pt;mso-border-left-themecolor:accent5;mso-border-left-themetint:
  153;mso-border-alt:solid #8EAADB .5pt;mso-border-themecolor:accent5;
  mso-border-themetint:153;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;tab-stops:88.45pt'>si</p>
  </td>
  <td width=116 valign=top style='width:88.3pt;border-top:none;border-left:
  none;border-bottom:solid #8EAADB 1.0pt;mso-border-bottom-themecolor:accent5;
  mso-border-bottom-themetint:153;border-right:solid #8EAADB 1.0pt;mso-border-right-themecolor:
  accent5;mso-border-right-themetint:153;mso-border-top-alt:solid #8EAADB .5pt;
  mso-border-top-themecolor:accent5;mso-border-top-themetint:153;mso-border-left-alt:
  solid #8EAADB .5pt;mso-border-left-themecolor:accent5;mso-border-left-themetint:
  153;mso-border-alt:solid #8EAADB .5pt;mso-border-themecolor:accent5;
  mso-border-themetint:153;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;tab-stops:88.45pt'>Id del producto a modificar</p>
  </td>
 </tr>
</table>

**Definición de atributos JSON de respuesta:**

<table class=MsoTable15Grid4Accent5 border=1 cellspacing=0 cellpadding=0
 style='border-collapse:collapse;border:none;mso-border-alt:solid #8EAADB .5pt;
 mso-border-themecolor:accent5;mso-border-themetint:153;mso-yfti-tbllook:1184;
 mso-padding-alt:0cm 5.4pt 0cm 5.4pt'>
 <tr style='mso-yfti-irow:-1;mso-yfti-firstrow:yes;mso-yfti-lastfirstrow:yes'>
  <td width=124 valign=top style='width:88.25pt;border:solid #4472C4 1.0pt;
  mso-border-themecolor:accent5;border-right:none;mso-border-top-alt:solid #4472C4 .5pt;
  mso-border-top-themecolor:accent5;mso-border-left-alt:solid #4472C4 .5pt;
  mso-border-left-themecolor:accent5;mso-border-bottom-alt:solid #4472C4 .5pt;
  mso-border-bottom-themecolor:accent5;background:#4472C4;mso-background-themecolor:
  accent5;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal;tab-stops:88.45pt;mso-yfti-cnfc:5'><b><span
  style='color:white;mso-themecolor:background1'>Atributo<o:p></o:p></span></b></p>
  </td>
  <td width=107 valign=top style='width:88.25pt;border-top:solid #4472C4 1.0pt;
  mso-border-top-themecolor:accent5;border-left:none;border-bottom:solid #4472C4 1.0pt;
  mso-border-bottom-themecolor:accent5;border-right:none;mso-border-top-alt:
  solid #4472C4 .5pt;mso-border-top-themecolor:accent5;mso-border-bottom-alt:
  solid #4472C4 .5pt;mso-border-bottom-themecolor:accent5;background:#4472C4;
  mso-background-themecolor:accent5;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal;tab-stops:88.45pt;mso-yfti-cnfc:1'><b><span
  style='color:white;mso-themecolor:background1'>Tipo de dato<o:p></o:p></span></b></p>
  </td>
  <td width=109 valign=top style='width:88.3pt;border-top:solid #4472C4 1.0pt;
  mso-border-top-themecolor:accent5;border-left:none;border-bottom:solid #4472C4 1.0pt;
  mso-border-bottom-themecolor:accent5;border-right:none;mso-border-top-alt:
  solid #4472C4 .5pt;mso-border-top-themecolor:accent5;mso-border-bottom-alt:
  solid #4472C4 .5pt;mso-border-bottom-themecolor:accent5;background:#4472C4;
  mso-background-themecolor:accent5;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal;tab-stops:88.45pt;mso-yfti-cnfc:1'><b><span
  style='color:white;mso-themecolor:background1'>Longitud <span class=SpellE>maxima</span><o:p></o:p></span></b></p>
  </td>
  <td width=111 valign=top style='width:88.3pt;border-top:solid #4472C4 1.0pt;
  mso-border-top-themecolor:accent5;border-left:none;border-bottom:solid #4472C4 1.0pt;
  mso-border-bottom-themecolor:accent5;border-right:none;mso-border-top-alt:
  solid #4472C4 .5pt;mso-border-top-themecolor:accent5;mso-border-bottom-alt:
  solid #4472C4 .5pt;mso-border-bottom-themecolor:accent5;background:#4472C4;
  mso-background-themecolor:accent5;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal;tab-stops:88.45pt;mso-yfti-cnfc:1'><b><span
  style='color:white;mso-themecolor:background1'>obligatorio<o:p></o:p></span></b></p>
  </td>
  <td width=115 valign=top style='width:88.3pt;border:solid #4472C4 1.0pt;
  mso-border-themecolor:accent5;border-left:none;mso-border-top-alt:solid #4472C4 .5pt;
  mso-border-top-themecolor:accent5;mso-border-bottom-alt:solid #4472C4 .5pt;
  mso-border-bottom-themecolor:accent5;mso-border-right-alt:solid #4472C4 .5pt;
  mso-border-right-themecolor:accent5;background:#4472C4;mso-background-themecolor:
  accent5;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal;tab-stops:88.45pt;mso-yfti-cnfc:1'><b><span
  style='color:white;mso-themecolor:background1'>Descripción <o:p></o:p></span></b></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:0'>
  <td width=124 valign=top style='width:88.25pt;border:solid #8EAADB 1.0pt;
  mso-border-themecolor:accent5;mso-border-themetint:153;border-top:none;
  mso-border-top-alt:solid #8EAADB .5pt;mso-border-top-themecolor:accent5;
  mso-border-top-themetint:153;mso-border-alt:solid #8EAADB .5pt;mso-border-themecolor:
  accent5;mso-border-themetint:153;background:#D9E2F3;mso-background-themecolor:
  accent5;mso-background-themetint:51;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;tab-stops:88.45pt;mso-yfti-cnfc:68'><span class=SpellE><span
  style='mso-bidi-font-weight:bold'>nombre_producto</span></span><span
  style='mso-bidi-font-weight:bold'><o:p></o:p></span></p>
  </td>
  <td width=107 valign=top style='width:88.25pt;border-top:none;border-left:
  none;border-bottom:solid #8EAADB 1.0pt;mso-border-bottom-themecolor:accent5;
  mso-border-bottom-themetint:153;border-right:solid #8EAADB 1.0pt;mso-border-right-themecolor:
  accent5;mso-border-right-themetint:153;mso-border-top-alt:solid #8EAADB .5pt;
  mso-border-top-themecolor:accent5;mso-border-top-themetint:153;mso-border-left-alt:
  solid #8EAADB .5pt;mso-border-left-themecolor:accent5;mso-border-left-themetint:
  153;mso-border-alt:solid #8EAADB .5pt;mso-border-themecolor:accent5;
  mso-border-themetint:153;background:#D9E2F3;mso-background-themecolor:accent5;
  mso-background-themetint:51;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;tab-stops:88.45pt;mso-yfti-cnfc:64'><span class=SpellE>string</span></p>
  </td>
  <td width=109 valign=top style='width:88.3pt;border-top:none;border-left:
  none;border-bottom:solid #8EAADB 1.0pt;mso-border-bottom-themecolor:accent5;
  mso-border-bottom-themetint:153;border-right:solid #8EAADB 1.0pt;mso-border-right-themecolor:
  accent5;mso-border-right-themetint:153;mso-border-top-alt:solid #8EAADB .5pt;
  mso-border-top-themecolor:accent5;mso-border-top-themetint:153;mso-border-left-alt:
  solid #8EAADB .5pt;mso-border-left-themecolor:accent5;mso-border-left-themetint:
  153;mso-border-alt:solid #8EAADB .5pt;mso-border-themecolor:accent5;
  mso-border-themetint:153;background:#D9E2F3;mso-background-themecolor:accent5;
  mso-background-themetint:51;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;tab-stops:88.45pt;mso-yfti-cnfc:64'>100</p>
  </td>
  <td width=111 valign=top style='width:88.3pt;border-top:none;border-left:
  none;border-bottom:solid #8EAADB 1.0pt;mso-border-bottom-themecolor:accent5;
  mso-border-bottom-themetint:153;border-right:solid #8EAADB 1.0pt;mso-border-right-themecolor:
  accent5;mso-border-right-themetint:153;mso-border-top-alt:solid #8EAADB .5pt;
  mso-border-top-themecolor:accent5;mso-border-top-themetint:153;mso-border-left-alt:
  solid #8EAADB .5pt;mso-border-left-themecolor:accent5;mso-border-left-themetint:
  153;mso-border-alt:solid #8EAADB .5pt;mso-border-themecolor:accent5;
  mso-border-themetint:153;background:#D9E2F3;mso-background-themecolor:accent5;
  mso-background-themetint:51;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;tab-stops:88.45pt;mso-yfti-cnfc:64'>Si</p>
  </td>
  <td width=115 valign=top style='width:88.3pt;border-top:none;border-left:
  none;border-bottom:solid #8EAADB 1.0pt;mso-border-bottom-themecolor:accent5;
  mso-border-bottom-themetint:153;border-right:solid #8EAADB 1.0pt;mso-border-right-themecolor:
  accent5;mso-border-right-themetint:153;mso-border-top-alt:solid #8EAADB .5pt;
  mso-border-top-themecolor:accent5;mso-border-top-themetint:153;mso-border-left-alt:
  solid #8EAADB .5pt;mso-border-left-themecolor:accent5;mso-border-left-themetint:
  153;mso-border-alt:solid #8EAADB .5pt;mso-border-themecolor:accent5;
  mso-border-themetint:153;background:#D9E2F3;mso-background-themecolor:accent5;
  mso-background-themetint:51;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;tab-stops:88.45pt;mso-yfti-cnfc:64'>Nombre del producto</p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:1'>
  <td width=124 valign=top style='width:88.25pt;border:solid #8EAADB 1.0pt;
  mso-border-themecolor:accent5;mso-border-themetint:153;border-top:none;
  mso-border-top-alt:solid #8EAADB .5pt;mso-border-top-themecolor:accent5;
  mso-border-top-themetint:153;mso-border-alt:solid #8EAADB .5pt;mso-border-themecolor:
  accent5;mso-border-themetint:153;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;tab-stops:88.45pt;mso-yfti-cnfc:4'><span style='mso-bidi-font-weight:
  bold'>precio<o:p></o:p></span></p>
  </td>
  <td width=107 valign=top style='width:88.25pt;border-top:none;border-left:
  none;border-bottom:solid #8EAADB 1.0pt;mso-border-bottom-themecolor:accent5;
  mso-border-bottom-themetint:153;border-right:solid #8EAADB 1.0pt;mso-border-right-themecolor:
  accent5;mso-border-right-themetint:153;mso-border-top-alt:solid #8EAADB .5pt;
  mso-border-top-themecolor:accent5;mso-border-top-themetint:153;mso-border-left-alt:
  solid #8EAADB .5pt;mso-border-left-themecolor:accent5;mso-border-left-themetint:
  153;mso-border-alt:solid #8EAADB .5pt;mso-border-themecolor:accent5;
  mso-border-themetint:153;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;tab-stops:88.45pt'>Doublé</p>
  </td>
  <td width=109 valign=top style='width:88.3pt;border-top:none;border-left:
  none;border-bottom:solid #8EAADB 1.0pt;mso-border-bottom-themecolor:accent5;
  mso-border-bottom-themetint:153;border-right:solid #8EAADB 1.0pt;mso-border-right-themecolor:
  accent5;mso-border-right-themetint:153;mso-border-top-alt:solid #8EAADB .5pt;
  mso-border-top-themecolor:accent5;mso-border-top-themetint:153;mso-border-left-alt:
  solid #8EAADB .5pt;mso-border-left-themecolor:accent5;mso-border-left-themetint:
  153;mso-border-alt:solid #8EAADB .5pt;mso-border-themecolor:accent5;
  mso-border-themetint:153;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;tab-stops:88.45pt'>-</p>
  </td>
  <td width=111 valign=top style='width:88.3pt;border-top:none;border-left:
  none;border-bottom:solid #8EAADB 1.0pt;mso-border-bottom-themecolor:accent5;
  mso-border-bottom-themetint:153;border-right:solid #8EAADB 1.0pt;mso-border-right-themecolor:
  accent5;mso-border-right-themetint:153;mso-border-top-alt:solid #8EAADB .5pt;
  mso-border-top-themecolor:accent5;mso-border-top-themetint:153;mso-border-left-alt:
  solid #8EAADB .5pt;mso-border-left-themecolor:accent5;mso-border-left-themetint:
  153;mso-border-alt:solid #8EAADB .5pt;mso-border-themecolor:accent5;
  mso-border-themetint:153;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;tab-stops:88.45pt'>Si</p>
  </td>
  <td width=115 valign=top style='width:88.3pt;border-top:none;border-left:
  none;border-bottom:solid #8EAADB 1.0pt;mso-border-bottom-themecolor:accent5;
  mso-border-bottom-themetint:153;border-right:solid #8EAADB 1.0pt;mso-border-right-themecolor:
  accent5;mso-border-right-themetint:153;mso-border-top-alt:solid #8EAADB .5pt;
  mso-border-top-themecolor:accent5;mso-border-top-themetint:153;mso-border-left-alt:
  solid #8EAADB .5pt;mso-border-left-themecolor:accent5;mso-border-left-themetint:
  153;mso-border-alt:solid #8EAADB .5pt;mso-border-themecolor:accent5;
  mso-border-themetint:153;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;tab-stops:88.45pt'>Precio del producto</p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:2'>
  <td width=124 valign=top style='width:88.25pt;border:solid #8EAADB 1.0pt;
  mso-border-themecolor:accent5;mso-border-themetint:153;border-top:none;
  mso-border-top-alt:solid #8EAADB .5pt;mso-border-top-themecolor:accent5;
  mso-border-top-themetint:153;mso-border-alt:solid #8EAADB .5pt;mso-border-themecolor:
  accent5;mso-border-themetint:153;background:#D9E2F3;mso-background-themecolor:
  accent5;mso-background-themetint:51;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;tab-stops:88.45pt;mso-yfti-cnfc:68'><span style='mso-bidi-font-weight:
  bold'>foto<o:p></o:p></span></p>
  </td>
  <td width=107 valign=top style='width:88.25pt;border-top:none;border-left:
  none;border-bottom:solid #8EAADB 1.0pt;mso-border-bottom-themecolor:accent5;
  mso-border-bottom-themetint:153;border-right:solid #8EAADB 1.0pt;mso-border-right-themecolor:
  accent5;mso-border-right-themetint:153;mso-border-top-alt:solid #8EAADB .5pt;
  mso-border-top-themecolor:accent5;mso-border-top-themetint:153;mso-border-left-alt:
  solid #8EAADB .5pt;mso-border-left-themecolor:accent5;mso-border-left-themetint:
  153;mso-border-alt:solid #8EAADB .5pt;mso-border-themecolor:accent5;
  mso-border-themetint:153;background:#D9E2F3;mso-background-themecolor:accent5;
  mso-background-themetint:51;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;tab-stops:88.45pt;mso-yfti-cnfc:64'>Blob</p>
  </td>
  <td width=109 valign=top style='width:88.3pt;border-top:none;border-left:
  none;border-bottom:solid #8EAADB 1.0pt;mso-border-bottom-themecolor:accent5;
  mso-border-bottom-themetint:153;border-right:solid #8EAADB 1.0pt;mso-border-right-themecolor:
  accent5;mso-border-right-themetint:153;mso-border-top-alt:solid #8EAADB .5pt;
  mso-border-top-themecolor:accent5;mso-border-top-themetint:153;mso-border-left-alt:
  solid #8EAADB .5pt;mso-border-left-themecolor:accent5;mso-border-left-themetint:
  153;mso-border-alt:solid #8EAADB .5pt;mso-border-themecolor:accent5;
  mso-border-themetint:153;background:#D9E2F3;mso-background-themecolor:accent5;
  mso-background-themetint:51;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;tab-stops:88.45pt;mso-yfti-cnfc:64'>-</p>
  </td>
  <td width=111 valign=top style='width:88.3pt;border-top:none;border-left:
  none;border-bottom:solid #8EAADB 1.0pt;mso-border-bottom-themecolor:accent5;
  mso-border-bottom-themetint:153;border-right:solid #8EAADB 1.0pt;mso-border-right-themecolor:
  accent5;mso-border-right-themetint:153;mso-border-top-alt:solid #8EAADB .5pt;
  mso-border-top-themecolor:accent5;mso-border-top-themetint:153;mso-border-left-alt:
  solid #8EAADB .5pt;mso-border-left-themecolor:accent5;mso-border-left-themetint:
  153;mso-border-alt:solid #8EAADB .5pt;mso-border-themecolor:accent5;
  mso-border-themetint:153;background:#D9E2F3;mso-background-themecolor:accent5;
  mso-background-themetint:51;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;tab-stops:88.45pt;mso-yfti-cnfc:64'>Si</p>
  </td>
  <td width=115 valign=top style='width:88.3pt;border-top:none;border-left:
  none;border-bottom:solid #8EAADB 1.0pt;mso-border-bottom-themecolor:accent5;
  mso-border-bottom-themetint:153;border-right:solid #8EAADB 1.0pt;mso-border-right-themecolor:
  accent5;mso-border-right-themetint:153;mso-border-top-alt:solid #8EAADB .5pt;
  mso-border-top-themecolor:accent5;mso-border-top-themetint:153;mso-border-left-alt:
  solid #8EAADB .5pt;mso-border-left-themecolor:accent5;mso-border-left-themetint:
  153;mso-border-alt:solid #8EAADB .5pt;mso-border-themecolor:accent5;
  mso-border-themetint:153;background:#D9E2F3;mso-background-themecolor:accent5;
  mso-background-themetint:51;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;tab-stops:88.45pt;mso-yfti-cnfc:64'>Fotografía del producto en base 64</p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:3'>
  <td width=124 valign=top style='width:88.25pt;border:solid #8EAADB 1.0pt;
  mso-border-themecolor:accent5;mso-border-themetint:153;border-top:none;
  mso-border-top-alt:solid #8EAADB .5pt;mso-border-top-themecolor:accent5;
  mso-border-top-themetint:153;mso-border-alt:solid #8EAADB .5pt;mso-border-themecolor:
  accent5;mso-border-themetint:153;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;tab-stops:88.45pt;mso-yfti-cnfc:4'><span style='mso-bidi-font-weight:
  bold'>existencia<o:p></o:p></span></p>
  </td>
  <td width=107 valign=top style='width:88.25pt;border-top:none;border-left:
  none;border-bottom:solid #8EAADB 1.0pt;mso-border-bottom-themecolor:accent5;
  mso-border-bottom-themetint:153;border-right:solid #8EAADB 1.0pt;mso-border-right-themecolor:
  accent5;mso-border-right-themetint:153;mso-border-top-alt:solid #8EAADB .5pt;
  mso-border-top-themecolor:accent5;mso-border-top-themetint:153;mso-border-left-alt:
  solid #8EAADB .5pt;mso-border-left-themecolor:accent5;mso-border-left-themetint:
  153;mso-border-alt:solid #8EAADB .5pt;mso-border-themecolor:accent5;
  mso-border-themetint:153;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;tab-stops:88.45pt'><span class=SpellE>Int</span></p>
  </td>
  <td width=109 valign=top style='width:88.3pt;border-top:none;border-left:
  none;border-bottom:solid #8EAADB 1.0pt;mso-border-bottom-themecolor:accent5;
  mso-border-bottom-themetint:153;border-right:solid #8EAADB 1.0pt;mso-border-right-themecolor:
  accent5;mso-border-right-themetint:153;mso-border-top-alt:solid #8EAADB .5pt;
  mso-border-top-themecolor:accent5;mso-border-top-themetint:153;mso-border-left-alt:
  solid #8EAADB .5pt;mso-border-left-themecolor:accent5;mso-border-left-themetint:
  153;mso-border-alt:solid #8EAADB .5pt;mso-border-themecolor:accent5;
  mso-border-themetint:153;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;tab-stops:88.45pt'>-</p>
  </td>
  <td width=111 valign=top style='width:88.3pt;border-top:none;border-left:
  none;border-bottom:solid #8EAADB 1.0pt;mso-border-bottom-themecolor:accent5;
  mso-border-bottom-themetint:153;border-right:solid #8EAADB 1.0pt;mso-border-right-themecolor:
  accent5;mso-border-right-themetint:153;mso-border-top-alt:solid #8EAADB .5pt;
  mso-border-top-themecolor:accent5;mso-border-top-themetint:153;mso-border-left-alt:
  solid #8EAADB .5pt;mso-border-left-themecolor:accent5;mso-border-left-themetint:
  153;mso-border-alt:solid #8EAADB .5pt;mso-border-themecolor:accent5;
  mso-border-themetint:153;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;tab-stops:88.45pt'>Si</p>
  </td>
  <td width=115 valign=top style='width:88.3pt;border-top:none;border-left:
  none;border-bottom:solid #8EAADB 1.0pt;mso-border-bottom-themecolor:accent5;
  mso-border-bottom-themetint:153;border-right:solid #8EAADB 1.0pt;mso-border-right-themecolor:
  accent5;mso-border-right-themetint:153;mso-border-top-alt:solid #8EAADB .5pt;
  mso-border-top-themecolor:accent5;mso-border-top-themetint:153;mso-border-left-alt:
  solid #8EAADB .5pt;mso-border-left-themecolor:accent5;mso-border-left-themetint:
  153;mso-border-alt:solid #8EAADB .5pt;mso-border-themecolor:accent5;
  mso-border-themetint:153;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;tab-stops:88.45pt'>Disponibilidad del producto</p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:4'>
  <td width=124 valign=top style='width:88.25pt;border:solid #8EAADB 1.0pt;
  mso-border-themecolor:accent5;mso-border-themetint:153;border-top:none;
  mso-border-top-alt:solid #8EAADB .5pt;mso-border-top-themecolor:accent5;
  mso-border-top-themetint:153;mso-border-alt:solid #8EAADB .5pt;mso-border-themecolor:
  accent5;mso-border-themetint:153;background:#D9E2F3;mso-background-themecolor:
  accent5;mso-background-themetint:51;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;tab-stops:88.45pt;mso-yfti-cnfc:68'><span class=SpellE><span
  style='mso-bidi-font-weight:bold'>tipo_producto</span></span><span
  style='mso-bidi-font-weight:bold'><o:p></o:p></span></p>
  </td>
  <td width=107 valign=top style='width:88.25pt;border-top:none;border-left:
  none;border-bottom:solid #8EAADB 1.0pt;mso-border-bottom-themecolor:accent5;
  mso-border-bottom-themetint:153;border-right:solid #8EAADB 1.0pt;mso-border-right-themecolor:
  accent5;mso-border-right-themetint:153;mso-border-top-alt:solid #8EAADB .5pt;
  mso-border-top-themecolor:accent5;mso-border-top-themetint:153;mso-border-left-alt:
  solid #8EAADB .5pt;mso-border-left-themecolor:accent5;mso-border-left-themetint:
  153;mso-border-alt:solid #8EAADB .5pt;mso-border-themecolor:accent5;
  mso-border-themetint:153;background:#D9E2F3;mso-background-themecolor:accent5;
  mso-background-themetint:51;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;tab-stops:88.45pt;mso-yfti-cnfc:64'><span class=SpellE>Int</span></p>
  </td>
  <td width=109 valign=top style='width:88.3pt;border-top:none;border-left:
  none;border-bottom:solid #8EAADB 1.0pt;mso-border-bottom-themecolor:accent5;
  mso-border-bottom-themetint:153;border-right:solid #8EAADB 1.0pt;mso-border-right-themecolor:
  accent5;mso-border-right-themetint:153;mso-border-top-alt:solid #8EAADB .5pt;
  mso-border-top-themecolor:accent5;mso-border-top-themetint:153;mso-border-left-alt:
  solid #8EAADB .5pt;mso-border-left-themecolor:accent5;mso-border-left-themetint:
  153;mso-border-alt:solid #8EAADB .5pt;mso-border-themecolor:accent5;
  mso-border-themetint:153;background:#D9E2F3;mso-background-themecolor:accent5;
  mso-background-themetint:51;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;tab-stops:88.45pt;mso-yfti-cnfc:64'>-</p>
  </td>
  <td width=111 valign=top style='width:88.3pt;border-top:none;border-left:
  none;border-bottom:solid #8EAADB 1.0pt;mso-border-bottom-themecolor:accent5;
  mso-border-bottom-themetint:153;border-right:solid #8EAADB 1.0pt;mso-border-right-themecolor:
  accent5;mso-border-right-themetint:153;mso-border-top-alt:solid #8EAADB .5pt;
  mso-border-top-themecolor:accent5;mso-border-top-themetint:153;mso-border-left-alt:
  solid #8EAADB .5pt;mso-border-left-themecolor:accent5;mso-border-left-themetint:
  153;mso-border-alt:solid #8EAADB .5pt;mso-border-themecolor:accent5;
  mso-border-themetint:153;background:#D9E2F3;mso-background-themecolor:accent5;
  mso-background-themetint:51;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;tab-stops:88.45pt;mso-yfti-cnfc:64'>Si</p>
  </td>
  <td width=115 valign=top style='width:88.3pt;border-top:none;border-left:
  none;border-bottom:solid #8EAADB 1.0pt;mso-border-bottom-themecolor:accent5;
  mso-border-bottom-themetint:153;border-right:solid #8EAADB 1.0pt;mso-border-right-themecolor:
  accent5;mso-border-right-themetint:153;mso-border-top-alt:solid #8EAADB .5pt;
  mso-border-top-themecolor:accent5;mso-border-top-themetint:153;mso-border-left-alt:
  solid #8EAADB .5pt;mso-border-left-themecolor:accent5;mso-border-left-themetint:
  153;mso-border-alt:solid #8EAADB .5pt;mso-border-themecolor:accent5;
  mso-border-themetint:153;background:#D9E2F3;mso-background-themecolor:accent5;
  mso-background-themetint:51;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;tab-stops:88.45pt;mso-yfti-cnfc:64'>Id del tipo de producto</p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:5;mso-yfti-lastrow:yes'>
  <td width=124 valign=top style='width:88.25pt;border:solid #8EAADB 1.0pt;
  mso-border-themecolor:accent5;mso-border-themetint:153;border-top:none;
  mso-border-top-alt:solid #8EAADB .5pt;mso-border-top-themecolor:accent5;
  mso-border-top-themetint:153;mso-border-alt:solid #8EAADB .5pt;mso-border-themecolor:
  accent5;mso-border-themetint:153;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;tab-stops:88.45pt;mso-yfti-cnfc:4'><span class=SpellE><span
  style='mso-bidi-font-weight:bold'>id_producto</span></span><span
  style='mso-bidi-font-weight:bold'><o:p></o:p></span></p>
  </td>
  <td width=107 valign=top style='width:88.25pt;border-top:none;border-left:
  none;border-bottom:solid #8EAADB 1.0pt;mso-border-bottom-themecolor:accent5;
  mso-border-bottom-themetint:153;border-right:solid #8EAADB 1.0pt;mso-border-right-themecolor:
  accent5;mso-border-right-themetint:153;mso-border-top-alt:solid #8EAADB .5pt;
  mso-border-top-themecolor:accent5;mso-border-top-themetint:153;mso-border-left-alt:
  solid #8EAADB .5pt;mso-border-left-themecolor:accent5;mso-border-left-themetint:
  153;mso-border-alt:solid #8EAADB .5pt;mso-border-themecolor:accent5;
  mso-border-themetint:153;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;tab-stops:88.45pt'><span class=SpellE>int</span></p>
  </td>
  <td width=109 valign=top style='width:88.3pt;border-top:none;border-left:
  none;border-bottom:solid #8EAADB 1.0pt;mso-border-bottom-themecolor:accent5;
  mso-border-bottom-themetint:153;border-right:solid #8EAADB 1.0pt;mso-border-right-themecolor:
  accent5;mso-border-right-themetint:153;mso-border-top-alt:solid #8EAADB .5pt;
  mso-border-top-themecolor:accent5;mso-border-top-themetint:153;mso-border-left-alt:
  solid #8EAADB .5pt;mso-border-left-themecolor:accent5;mso-border-left-themetint:
  153;mso-border-alt:solid #8EAADB .5pt;mso-border-themecolor:accent5;
  mso-border-themetint:153;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;tab-stops:88.45pt'>-</p>
  </td>
  <td width=111 valign=top style='width:88.3pt;border-top:none;border-left:
  none;border-bottom:solid #8EAADB 1.0pt;mso-border-bottom-themecolor:accent5;
  mso-border-bottom-themetint:153;border-right:solid #8EAADB 1.0pt;mso-border-right-themecolor:
  accent5;mso-border-right-themetint:153;mso-border-top-alt:solid #8EAADB .5pt;
  mso-border-top-themecolor:accent5;mso-border-top-themetint:153;mso-border-left-alt:
  solid #8EAADB .5pt;mso-border-left-themecolor:accent5;mso-border-left-themetint:
  153;mso-border-alt:solid #8EAADB .5pt;mso-border-themecolor:accent5;
  mso-border-themetint:153;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;tab-stops:88.45pt'>si</p>
  </td>
  <td width=115 valign=top style='width:88.3pt;border-top:none;border-left:
  none;border-bottom:solid #8EAADB 1.0pt;mso-border-bottom-themecolor:accent5;
  mso-border-bottom-themetint:153;border-right:solid #8EAADB 1.0pt;mso-border-right-themecolor:
  accent5;mso-border-right-themetint:153;mso-border-top-alt:solid #8EAADB .5pt;
  mso-border-top-themecolor:accent5;mso-border-top-themetint:153;mso-border-left-alt:
  solid #8EAADB .5pt;mso-border-left-themecolor:accent5;mso-border-left-themetint:
  153;mso-border-alt:solid #8EAADB .5pt;mso-border-themecolor:accent5;
  mso-border-themetint:153;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;tab-stops:88.45pt'>Id del producto a modificar</p>
  </td>
 </tr>
</table>

Ejemplo de uso

Entrada:

url: api/productos

JSON:

![enter image description here](https://i.gyazo.com/6b5dc65b69376077ef52d88ccdebd789.png)

Salidas:

En caso de éxito:

Http Status: 200

En caso de éxito:

![enter image description here](https://i.gyazo.com/2bbd376a9649c1ce4c1a510bcbaa54a7.png)

En caso de fallo:

Http Status: 400

## Servicio DELETE

**Nombre del servicio:** Productos

**Nombre del método:** productos

**URL:** /api/productos/id

**Tipo de HTTP Method:** DELETE

**Definición de atributos JSON de respuesta:**

<table class=MsoTable15Grid4Accent5 border=1 cellspacing=0 cellpadding=0
 style='border-collapse:collapse;border:none;mso-border-alt:solid #8EAADB .5pt;
 mso-border-themecolor:accent5;mso-border-themetint:153;mso-yfti-tbllook:1184;
 mso-padding-alt:0cm 5.4pt 0cm 5.4pt'>
 <tr style='mso-yfti-irow:-1;mso-yfti-firstrow:yes;mso-yfti-lastfirstrow:yes'>
  <td width=120 valign=top style='width:92.85pt;border:solid #4472C4 1.0pt;
  mso-border-themecolor:accent5;border-right:none;mso-border-top-alt:solid #4472C4 .5pt;
  mso-border-top-themecolor:accent5;mso-border-left-alt:solid #4472C4 .5pt;
  mso-border-left-themecolor:accent5;mso-border-bottom-alt:solid #4472C4 .5pt;
  mso-border-bottom-themecolor:accent5;background:#4472C4;mso-background-themecolor:
  accent5;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal;tab-stops:88.45pt;mso-yfti-cnfc:5'><b><span
  style='color:white;mso-themecolor:background1'>Atributo<o:p></o:p></span></b></p>
  </td>
  <td width=108 valign=top style='width:86.55pt;border-top:solid #4472C4 1.0pt;
  mso-border-top-themecolor:accent5;border-left:none;border-bottom:solid #4472C4 1.0pt;
  mso-border-bottom-themecolor:accent5;border-right:none;mso-border-top-alt:
  solid #4472C4 .5pt;mso-border-top-themecolor:accent5;mso-border-bottom-alt:
  solid #4472C4 .5pt;mso-border-bottom-themecolor:accent5;background:#4472C4;
  mso-background-themecolor:accent5;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal;tab-stops:88.45pt;mso-yfti-cnfc:1'><b><span
  style='color:white;mso-themecolor:background1'>Tipo de dato<o:p></o:p></span></b></p>
  </td>
  <td width=111 valign=top style='width:86.9pt;border-top:solid #4472C4 1.0pt;
  mso-border-top-themecolor:accent5;border-left:none;border-bottom:solid #4472C4 1.0pt;
  mso-border-bottom-themecolor:accent5;border-right:none;mso-border-top-alt:
  solid #4472C4 .5pt;mso-border-top-themecolor:accent5;mso-border-bottom-alt:
  solid #4472C4 .5pt;mso-border-bottom-themecolor:accent5;background:#4472C4;
  mso-background-themecolor:accent5;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal;tab-stops:88.45pt;mso-yfti-cnfc:1'><b><span
  style='color:white;mso-themecolor:background1'>Longitud <span class=SpellE>maxima</span><o:p></o:p></span></b></p>
  </td>
  <td width=113 valign=top style='width:87.3pt;border-top:solid #4472C4 1.0pt;
  mso-border-top-themecolor:accent5;border-left:none;border-bottom:solid #4472C4 1.0pt;
  mso-border-bottom-themecolor:accent5;border-right:none;mso-border-top-alt:
  solid #4472C4 .5pt;mso-border-top-themecolor:accent5;mso-border-bottom-alt:
  solid #4472C4 .5pt;mso-border-bottom-themecolor:accent5;background:#4472C4;
  mso-background-themecolor:accent5;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal;tab-stops:88.45pt;mso-yfti-cnfc:1'><b><span
  style='color:white;mso-themecolor:background1'>obligatorio<o:p></o:p></span></b></p>
  </td>
  <td width=114 valign=top style='width:87.8pt;border:solid #4472C4 1.0pt;
  mso-border-themecolor:accent5;border-left:none;mso-border-top-alt:solid #4472C4 .5pt;
  mso-border-top-themecolor:accent5;mso-border-bottom-alt:solid #4472C4 .5pt;
  mso-border-bottom-themecolor:accent5;mso-border-right-alt:solid #4472C4 .5pt;
  mso-border-right-themecolor:accent5;background:#4472C4;mso-background-themecolor:
  accent5;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal;tab-stops:88.45pt;mso-yfti-cnfc:1'><b><span
  style='color:white;mso-themecolor:background1'>Descripción <o:p></o:p></span></b></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:0;mso-yfti-lastrow:yes'>
  <td width=120 valign=top style='width:92.85pt;border:solid #8EAADB 1.0pt;
  mso-border-themecolor:accent5;mso-border-themetint:153;border-top:none;
  mso-border-top-alt:solid #8EAADB .5pt;mso-border-top-themecolor:accent5;
  mso-border-top-themetint:153;mso-border-alt:solid #8EAADB .5pt;mso-border-themecolor:
  accent5;mso-border-themetint:153;background:#D9E2F3;mso-background-themecolor:
  accent5;mso-background-themetint:51;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;tab-stops:88.45pt;mso-yfti-cnfc:68'><span class=SpellE><span
  style='mso-bidi-font-weight:bold'>id_producto</span></span><span
  style='mso-bidi-font-weight:bold'><o:p></o:p></span></p>
  </td>
  <td width=108 valign=top style='width:86.55pt;border-top:none;border-left:
  none;border-bottom:solid #8EAADB 1.0pt;mso-border-bottom-themecolor:accent5;
  mso-border-bottom-themetint:153;border-right:solid #8EAADB 1.0pt;mso-border-right-themecolor:
  accent5;mso-border-right-themetint:153;mso-border-top-alt:solid #8EAADB .5pt;
  mso-border-top-themecolor:accent5;mso-border-top-themetint:153;mso-border-left-alt:
  solid #8EAADB .5pt;mso-border-left-themecolor:accent5;mso-border-left-themetint:
  153;mso-border-alt:solid #8EAADB .5pt;mso-border-themecolor:accent5;
  mso-border-themetint:153;background:#D9E2F3;mso-background-themecolor:accent5;
  mso-background-themetint:51;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;tab-stops:88.45pt;mso-yfti-cnfc:64'><span class=SpellE>int</span></p>
  </td>
  <td width=111 valign=top style='width:86.9pt;border-top:none;border-left:
  none;border-bottom:solid #8EAADB 1.0pt;mso-border-bottom-themecolor:accent5;
  mso-border-bottom-themetint:153;border-right:solid #8EAADB 1.0pt;mso-border-right-themecolor:
  accent5;mso-border-right-themetint:153;mso-border-top-alt:solid #8EAADB .5pt;
  mso-border-top-themecolor:accent5;mso-border-top-themetint:153;mso-border-left-alt:
  solid #8EAADB .5pt;mso-border-left-themecolor:accent5;mso-border-left-themetint:
  153;mso-border-alt:solid #8EAADB .5pt;mso-border-themecolor:accent5;
  mso-border-themetint:153;background:#D9E2F3;mso-background-themecolor:accent5;
  mso-background-themetint:51;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;tab-stops:88.45pt;mso-yfti-cnfc:64'>-</p>
  </td>
  <td width=113 valign=top style='width:87.3pt;border-top:none;border-left:
  none;border-bottom:solid #8EAADB 1.0pt;mso-border-bottom-themecolor:accent5;
  mso-border-bottom-themetint:153;border-right:solid #8EAADB 1.0pt;mso-border-right-themecolor:
  accent5;mso-border-right-themetint:153;mso-border-top-alt:solid #8EAADB .5pt;
  mso-border-top-themecolor:accent5;mso-border-top-themetint:153;mso-border-left-alt:
  solid #8EAADB .5pt;mso-border-left-themecolor:accent5;mso-border-left-themetint:
  153;mso-border-alt:solid #8EAADB .5pt;mso-border-themecolor:accent5;
  mso-border-themetint:153;background:#D9E2F3;mso-background-themecolor:accent5;
  mso-background-themetint:51;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;tab-stops:88.45pt;mso-yfti-cnfc:64'>si</p>
  </td>
  <td width=114 valign=top style='width:87.8pt;border-top:none;border-left:
  none;border-bottom:solid #8EAADB 1.0pt;mso-border-bottom-themecolor:accent5;
  mso-border-bottom-themetint:153;border-right:solid #8EAADB 1.0pt;mso-border-right-themecolor:
  accent5;mso-border-right-themetint:153;mso-border-top-alt:solid #8EAADB .5pt;
  mso-border-top-themecolor:accent5;mso-border-top-themetint:153;mso-border-left-alt:
  solid #8EAADB .5pt;mso-border-left-themecolor:accent5;mso-border-left-themetint:
  153;mso-border-alt:solid #8EAADB .5pt;mso-border-themecolor:accent5;
  mso-border-themetint:153;background:#D9E2F3;mso-background-themecolor:accent5;
  mso-background-themetint:51;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;tab-stops:88.45pt;mso-yfti-cnfc:64'>Id del producto a modificar</p>
  </td>
 </tr>
</table>

Ejemplo de uso

Entrada:

url: api/productos/1

JSON: NULL

Salidas:

En caso de éxito:

Http Status: 200

En caso de éxito:

![enter image description here](https://i.gyazo.com/dd3568ea2f52be7440d758aa2e6e8f2c.png)

En caso de fallo:

Http Status: 404

