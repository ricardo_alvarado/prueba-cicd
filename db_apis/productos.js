const database = require('../services/database');

const baseQuery =
    `SELECT
        p.id_producto "id",
        p.nombre_producto "nombre_producto",
        p.precio "precio",
        p.fotografia "fotografia",
        p.existencia "existencia",
        t.id_tipo_producto "id_tipo_producto",
        t.tipo_producto "tipo_nombre"
    FROM 
        producto p,
        tipo_producto t
    WHERE 
        p.tipo_producto = t.id_tipo_producto`;
    
async function encontrar(context) {
    let query = baseQuery;
    const binds = [];
    
    if(context.id) {
        binds.push(context.id);
        query += `\nAND 
                      p.id_producto = ?`;
    }
    
    const result = await database.simpleExecute(query, binds);
    
    return result[0];
}

module.exports.encontrar = encontrar;

const createSql = 
    `INSERT INTO producto(
        nombre_producto,
        precio,
        fotografia,
        existencia,
        tipo_producto
    ) VALUES (?,?,?,?,?)`;
    
async function crear(emp) {
    const producto = Object.assign({}, emp);
    
    const productoNuevo = [emp.nombre_producto, emp.precio, emp.fotografia, emp.existencia, emp.tipo_producto];
    
    const result = await database.simpleExecute(createSql, productoNuevo);
    
    return producto;
}

module.exports.crear = crear;

const updateSql = 
    `UPDATE producto SET nombre_producto = ?, precio = ?, fotografia = ?, existencia = ?, tipo_producto = ?
    WHERE id_producto = ?`;

async function actualizar(emp) {
    const producto = Object.assign({}, emp);
    
    const productoActualizar = [emp.nombre_producto, emp.precio, emp.fotografia, emp.existencia, emp.tipo_producto, emp.id_producto];
    
    const result = await database.simpleExecute(updateSql, productoActualizar);
    
    if(result[0].affectedRows && result[0].affectedRows === 1) {
        return producto;
    } else {
        return null;
    }
}

module.exports.actualizar = actualizar;

const deleteSql = 
    `UPDATE producto SET existencia = 0
    WHERE id_producto = ?`;

async function borrar(emp) {
    const producto = Object.assign({}, emp);
    
    const productoBorrar = [emp.id_producto]
    
    const result = await database.simpleExecute(deleteSql, productoBorrar);
    
    if(result[0].affectedRows && result[0].affectedRows === 1) {
        return producto;
    } else {
        return null;
    }
}

module.exports.borrar = borrar;