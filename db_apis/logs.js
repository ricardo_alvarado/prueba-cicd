const database = require('../services/database');

const createSql = 
    `INSERT INTO flog(
        fecha,
        tipo_accion,
        id_usuario,
        nombre_usuario,
        nivel_accion,
        origen
    ) VALUES (?,?,?,?,?,?)`;
    
async function crear(emp) {
    const flog = Object.assign({}, emp);
    
    const logNuevo = [emp.fecha, emp.tipo_accion, emp.id_usuario, emp.nombre_usuario, emp.nivel_accion, emp.origen];
    
    const result = await database.simpleExecute(createSql, logNuevo);
    
    return flog;
}

module.exports.crear = crear;