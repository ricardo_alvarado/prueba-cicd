const database = require('../services/database');

const createSql = 
    `INSERT INTO ferror(
        fecha,
        id_producto,
        nivel_error,
        descripcion_error,
        stacktrace,
        tipo_accion,
        origen
    ) VALUES (?,?,?,?,?,?,?)`;
    
async function crear(emp) {
    const ferror = Object.assign({}, emp);
    
    const errorNuevo = [emp.fecha, emp.id_producto, emp.nivel_error, emp.descripcion_error, emp.stacktrace, emp.tipo_accion, emp.origen];
    
    const result = await database.simpleExecute(createSql, errorNuevo);
    
    return ferror;
}


module.exports.crear = crear;