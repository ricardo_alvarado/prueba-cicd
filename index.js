const database = require('./services/database');
const webServer = require('./services/server');
const dbConfig = require('./config/database');

process.env.UV_THREADPOLL_SIZE = dbConfig.pool.connectionLimit;

async function startup() {
    console.log('iniciando aplicacion');
    
    try {
        console.log('iniciando modulo de base de datos');
        
        await database.initialize();
    } catch(err) {
        console.log(err);
        
        process.exit(1);
    }
    
    try {
        console.log('iniciando servidor web');
        
        await webServer.initialize();
    } catch (err) {
        console.log('iniciando servidor web');
        
        process.exit(1);
    }
}

startup();