const productos = require('../db_apis/productos');
const ferrores = require('../db_apis/errores');
const flogs = require('../db_apis/logs');

function getRandomArbitrary(min = 100000, max = 999999) {
    return Math.floor(Math.random() * (max - min) + min);
}

async function get(req, res, next) {
    try {
        const context = {};
        
        context.id = parseInt(req.params.id, 10);
        
        const filas = await productos.encontrar(context);
        
        if(req.params.id) {
            if(filas.length === 1) {
                newLog(new Date().toISOString().slice(0, 19).replace('T', ' '), 'Lectura', 'Microservicio de Productos');
                res.status(200).json(filas[0]);
            } else {
                let errorNuevo = {
                    fecha: new Date().toISOString().slice(0, 19).replace('T', ' '),
                    id_producto: req.params.id,
                    nivel_error: 'usuario',
                    descripcion_error: 'Producto no encontrado',
                    stacktrace: 'Productos route get find 404',
                    tipo_accion: 'GET',
                    origen: 'Microservicio de Lectura Producto'
                }
                
                errorNuevo = await ferrores.crear(errorNuevo);
                
                res.status(404).end();
            }
        } else {
            newLog(new Date().toISOString().slice(0, 19).replace('T', ' '), 'Lectura', 'Microservicio de Productos');
            res.status(200).json(filas);
        }
    } catch(err) {
        let errorNuevo = {
            fecha: new Date().toISOString().slice(0, 19).replace('T', ' '),
            id_producto: -1,
            nivel_error: 'Usuario',
            descripcion_error: 'Error en el sistema (GET)',
            stacktrace: 'Error dice: ' + err,
            tipo_accion: 'GET',
            origen: 'Microservicio de Lectura Producto'
        }
        
        errorNuevo = await ferrores.crear(errorNuevo);
        
        next(err);
    }
}

module.exports.get = get;

async function post(req, res, next) {
    try{
        let basePath = 'http://104.131.99.203:8080/uploads/';
        
        let productoNuevo = {
            nombre_producto: req.body.nombre,
            precio: req.body.precio,
            fotografia: req.body.foto,
            existencia: req.body.existencia,
            tipo_producto: req.body.tipo
        }
        
        let tiempo = Date.now();
        let fechaHoy = new Date(tiempo);
        fechaHoy = fechaHoy.toISOString().slice(0, 19).split('T')[0].replace(/-/g, "_");

        let base64Data = productoNuevo.fotografia.split(',')[1];

        let tipoImagen = productoNuevo.fotografia.split('/')[1].split(';')[0];

        let nombreImagen = fechaHoy + getRandomArbitrary() + '.' + tipoImagen;
        
        productoNuevo.fotografia = basePath + nombreImagen;
        
        productoNuevo = await productos.crear(productoNuevo);
        
        require("fs").writeFile('./uploads/' + nombreImagen, base64Data, 'base64', function(err) {
            console.log(err);
        });
        newLog(new Date().toISOString().slice(0, 19).replace('T', ' '), 'Crear', 'Microservicio de Productos');
        res.status(201).json(productoNuevo);
    } catch(err) {
        let errorNuevo = {
            fecha: new Date().toISOString().slice(0, 19).replace('T', ' '),
            id_producto: -1,
            nivel_error: 'Sistema',
            descripcion_error: 'Error en el sistema (POST)',
            stacktrace: 'Error dice: ' + err,
            tipo_accion: 'POST',
            origen: 'Microservicio de Insertar Producto'
        }
        
        errorNuevo = await ferrores.crear(errorNuevo);
        next(err);
    }
}

module.exports.post = post;

async function put(req, res, next) {
    try {
        let productoActualizar = {
            nombre_producto: req.body.nombre,
            precio: req.body.precio,
            fotografia: req.body.foto,
            existencia: req.body.existencia,
            tipo_producto: req.body.tipo,
            id_producto: req.body.id
        }
        
        productoActualizar = await productos.actualizar(productoActualizar);
        
        if (productoActualizar !== null) {
            newLog(new Date().toISOString().slice(0, 19).replace('T', ' '), 'Actualizar', 'Microservicio de Productos');
            res.status(200).json(productoActualizar);
        } else {
            let errorNuevo = {
                fecha: new Date().toISOString().slice(0, 19).replace('T', ' '),
                id_producto: req.body.id,
                nivel_error: 'usuario',
                descripcion_error: 'Error en el sistema (PUT)',
                stacktrace: 'Productos put actualizar 400',
                tipo_accion: 'PUT',
                origen: 'Microservicio de Actualizar Producto'
            }
            
            errorNuevo = await ferrores.crear(errorNuevo);
            res.status(400).end();
        }
    } catch(err) {
        let errorNuevo = {
            fecha: new Date().toISOString().slice(0, 19).replace('T', ' '),
            id_producto: -1,
            nivel_error: 'Sistema',
            descripcion_error: 'Error en el sistema (PUT)',
            stacktrace: 'Error dice: ' + err,
            tipo_accion: 'PUT',
            origen: 'Microservicio de Actualizar Producto'
        }
        
        errorNuevo = await ferrores.crear(errorNuevo);
        next(err);
    }
}

module.exports.put = put;

async function destroy(req, res, next) {
    try {
        let productoBorrar = {};
        
        productoBorrar.id_producto = parseInt(req.params.id, 10);
        
        productoBorrar = await productos.borrar(productoBorrar);
        
        if(productoBorrar !== null) {
            newLog(new Date().toISOString().slice(0, 19).replace('T', ' '), 'Borrar', 'Microservicio de Productos');
            res.status(200).json(productoBorrar);
        } else {
            let errorNuevo = {
                fecha: new Date().toISOString().slice(0, 19).replace('T', ' '),
                id_producto: req.body.id,
                nivel_error: 'usuario',
                descripcion_error: 'Error en el sistema (DELETE)',
                stacktrace: 'Productos destroy borrar 400',
                tipo_accion: 'PUT',
                origen: 'Microservicio de Borrar Producto'
            }
            
            errorNuevo = await ferrores.crear(errorNuevo);
            res.status(404).end();
        }
    } catch(err) {
        let errorNuevo = {
            fecha: new Date().toISOString().slice(0, 19).replace('T', ' '),
            id_producto: -1,
            nivel_error: 'Sistema',
            descripcion_error: 'Error en el sistema (DELETE)',
            stacktrace: 'Error dice: ' + err,
            tipo_accion: 'DELETE',
            origen: 'Microservicio de Borrar Producto'
        }
        
        errorNuevo = await ferrores.crear(errorNuevo);
        next(err);
    }
}

module.exports.destroy = destroy;

function newLog(fecha, accion, origen) {
    let logNuevo = {
        fecha: fecha,
        tipo_accion: accion,
        id_usuario: 0,
        nombre_usuario: 'system',
        nivel_accion: 'Usuario',
        origen: origen
    }
    
    logNuevo = flogs.crear(logNuevo);
}